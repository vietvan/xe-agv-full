#ifndef Mp3_h
#define Mp3_h

#include "Arduino.h"

class Mp3
{
  private:

    byte mode,list,complete,_play,_next = 41; //mode 0:off     1:on    2:next
    unsigned long count;
    const int DELAY = 70; // Button delay and loading 0.1 sec
    const int LOAD = 500;
    const int PRESS = 0;
    const int OFF = 1;
    
  public:
    void init(int play/*,int next*/);
    void on();
    void off();
    void check();
    void next();
    void reset();
};
#endif



