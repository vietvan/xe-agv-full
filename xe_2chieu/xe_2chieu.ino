#include <MFRC522.h>
#include <SPI.h>
#include <PID_v1.h>
#include <EEPROM.h>
//#include "clashpoint.h"
double Setpoint = 315, Input, Output;
double Kp = 1.2, Ki = 0.0 , Kd = 0.029;
PID myPID(&Input, &Output, &Setpoint, Kp, Ki, Kd, DIRECT);

#include "D:/xe  agv full/Header.h"
#include "D:/xe  agv full/InfomationCode.h"

#define UP HIGH
#define DOWN LOW
# define START3 22
# define START4 23
# define EMG2 24
#define LAMP1 25
#define LAMP2 26
#define LAMP3 27
#define LAMP4 28
#define SPK 29
#define LIFTUP1 32
#define LIFTUP2 33
#define LIFTDOWN1 30

#define FRONT_RIGHT 34
#define FRONT_LEFT 35
#define BACK_RIGHT 36
#define BACK_LEFT 37
#define FRONT_SAFETY 38
#define FRONT_WARNING 39
#define BACK_WARNING 40
#define BACK_SAFETY 41
#define LIFT_PWM 44
#define LIFT_DIR 31

# define _RM 13
# define _RBRK 12
# define _LM 11
# define _LBRK 10
# define _LALM 9
# define LD 8
# define RD 7
# define _RALM 6
# define START1 5
# define START2 4
# define EMG1 3
# define LIFT_REST 2

# define FRONT_LINE_ANALOG A2
# define FRONT_LINE_DETECT A3
# define BACK_LINE_ANALOG A4
# define BACK_LINE_DETECT A5
// Define SPI
# define SS_PIN 53
# define RST_PIN 49
//event

#define BEGINSPEED 10
# define EVENT_TURNLEFT_IN3 14 // normal
# define EVENT_TURNLEFT_IN2 13 // boost
# define EVENT_TURNLEFT_IN1 10 // slow
# define EVENT_TURNLEFT_IN 11 // turn
# define EVENT_TURNLEFT_OUT 12

# define EVENT_TURNRIGHT_IN3 24
# define EVENT_TURNRIGHT_IN2 23
# define EVENT_TURNRIGHT_IN 21
# define EVENT_TURNRIGHT_IN1 20
# define EVENT_TURNRIGHT_OUT 22
# define EVENT_STOP 4
# define NO_EVENT 0
# define EVENT_SPEEDDROP 32
# define EVENT_BOOST 31
# define EVENT_SLOW 32
# define EVENT_NORMAL 30
# define EVENT_TURN 33
#define BACK_TURNRIGHT_NORMAL 25
#define BACK_TURNLEFT_NORMAL 26
#define FRONT_TURNRIGHT_NORMAL 27
#define FRONT_TURNLEFT_NORMAL 28

# define _NORMAL 50//100
# define _DEFAULT 50//100
# define _TURN 50//80
# define _BOOST 50
//130
# define _SLOW 31

# define EPPROM_NAME 0
# define EPPROM_LEFTADD 1
# define EPPROM_RIGHTADD 2
#define EPPROM_LINECHOOSE 10
#define MUSICBUTTONDELAY 80

int RM;
int RBRK;
int LM;
int LBRK;
int LALM;
int RALM;
int FORK_L;
int FORK_R;
int LINETRACK;
int LINEANALOG;
int SAFETY;
int WARNING;
bool frontSelect, backSelect, switchFrontFlag = false, switchBackFlag = false, slowFlag = false;

bool checkSwitchFlag1 = false, checkSwitchFlag2 = false, frontDirect;
unsigned long checkSwitchFlag1Time = 0;
int smaxr = 0;
int smaxl = 0;
void StopAGV();
void liftUp();
void liftDown();
void resetLift();
void frontSetup();
void backSetup();

#define SWITCH_SLOW_TIME 500

bool currentLift = DOWN;
bool musicFlag = false;
unsigned long musicTimer = 0;

unsigned long recallPrevTime = millis();

MFRC522 mfrc522(SS_PIN, RST_PIN);



byte closeStatus = 0;
//Specify the links and initial tuning parameters
bool speakerFlag = false, currentSpeedFlag = false;

int pos = 0;

const byte NAME = EEPROM.read(EPPROM_NAME);
const byte LEFTADD = EEPROM.read(EPPROM_LEFTADD);
const byte RIGHTADD = EEPROM.read(EPPROM_RIGHTADD);
byte closeLampReason = EEPROM.read(99);
int smaxrPrev = 0;
int smaxlPrev = 0;
unsigned long CardID = 1000, CardIDTemp;    // Hien thi so UID dang thap phan

int sdow = 0;
int Startdelay = 1000;

bool rightALM = HIGH, leftALM = HIGH;
int NORMALSPEED = _NORMAL;
int DEFAULTSPEED = _DEFAULT;
int TURNSPEED = _TURN;
int BOOSTSPEED = _BOOST;
int SLOWSPEED = _SLOW;
double currentSpeed, pointValue = 0;
double valueR;
double valueL;
double prevSpeed = 0;
unsigned long  stopCard = 0 , prevStopCard = 0;
unsigned long prevCard = 1000;
const unsigned long  cardNo1 = 1870775848;
const unsigned long cardNo2 = 1863600424;
const unsigned long cardNo3 = 1396368686;
const unsigned long cardNo4 = 1671576103;
const unsigned long  cardNo5 = 656470381;
const unsigned long cardNo6 = 930540397;
const unsigned long cardNo8 = 661132654;
const unsigned long cardNo9 = 1731786093;
const unsigned long cardNo10 = 3072335214;
const unsigned long cardNo11 = 1607974440;
const unsigned long cardNo12 = 666823789;

const unsigned long card1 = 2501389354;
const unsigned long card2 = 4121118762;
const unsigned long card3 = 1170947626;
const unsigned long card4 = 1701856298;
const unsigned long card5 = 3845799466;
const unsigned long card6 = 2231908394;
const unsigned long card7 = 2779066922;
const unsigned long card8 = 1438101265;
const unsigned long card9 = 3037704209;
const unsigned long card10 = 2510962218;
const unsigned long card11 = 2512206378;
const unsigned long card12 = 2775725354;
const unsigned long card13 = 3848416298;
const unsigned long card14 = 2514959914;
const unsigned long card15 = 903162410;
const unsigned long card16 = 2512011050;
const unsigned long card17 = 352469034;
const unsigned long card18 = 3844025386;
const unsigned long card19 = 1709001514;
const unsigned long card20 = 2504862762;
const unsigned long card21 = 362983185;
const unsigned long card22 = 634209066;
const unsigned long card23 = 3047698730;
const unsigned long card24 = 1161770282;
const unsigned long card25 = 366489130;
const unsigned long card26 = 364656938;
const unsigned long card27 = 1964586026;
const unsigned long card28 = 1709649706;
const unsigned long card29 = 2769108266;
const unsigned long card30 = 3042657322;
const unsigned long card31 = 636628778;
const unsigned long card32 = 895697706;
const unsigned long card33 = 3038262826;
const unsigned long card34 = 4126224682;
const unsigned long card35 = 3585289002;
const unsigned long card36 = 2502903850;
const unsigned long card37 = 2242262826;
const unsigned long card38 = 1965928465;
const unsigned long card39 = 2510631210;
const unsigned long card40 = 3038727210;
const unsigned long card41 = 2378078765;
const unsigned long cardnv = 3297683241;

const unsigned long  IDNo1 = 1345708295;
const unsigned long  IDNo2 = 2169183753;
const unsigned long  IDNo3 = 183269376;
const unsigned long  IDNo4 = 1671576103
                             ;
const unsigned long  IDNo5 = 1917949220;
const unsigned long  IDNo6 = 1344967687;
const unsigned long  IDNo7 = 1343335687;
const unsigned long  IDNo8 = 1351433991;
const unsigned long  IDNo9 = 1344585479;
const unsigned long  IDNo10 = 1350706183;
const unsigned long  IDNo11 = 867234087;
const unsigned long  IDNo12 = 870565415;
const unsigned long  IDNo13 = 865235751;
const unsigned long  IDNo14 = 851230244;
const unsigned long  IDNo15 = 1660064036;
const unsigned long  IDNo16 = 866554151;
const unsigned long  IDNo17 = 872260647;
const unsigned long  IDNo18 = 865552935;
const unsigned long  IDNo19 = 1126539815;
const unsigned long  IDNo20 = 869715495;
const unsigned long  IDNo21 = 865366567;
const unsigned long  IDNo22 = 1129932327;
const unsigned long  IDNo23 = 1126712359;
const unsigned long  IDNo24 = 864580135;
const unsigned long  IDNo25 = 867848231;
const unsigned long  IDNo26 = 1127835431;
const unsigned long  IDNo27 = 865686055;
const unsigned long  IDNo28 = 1128798247;
const unsigned long  IDNo29 = 868724263;
const unsigned long  IDNo30 = 869454119;
const unsigned long  IDNo31 = 1404991527;
const unsigned long  IDNo32 = 1407677223;
const unsigned long  IDNo33 = 904892205;
const unsigned long  IDNo34 = 1404290087;
const unsigned long  IDNo35 = 1124592167;
const unsigned long  IDNo36 = 909560866;
const unsigned long  IDNo37 = 1917504292;
const unsigned long  IDNo38 = 1129588263;
const unsigned long  IDNo39 = 1125363751;
const unsigned long IDNo40 = 2378078765;
const unsigned long IDNo41 = 1396368686;
const unsigned long IDNo42 = 112218898;
const unsigned long IDNo43 = 2691771556;
const unsigned long IDNo44 = 1129837351;
const unsigned long IDNo45 = 644198946;
const unsigned long IDNo46 = 649376290;
const unsigned long IDNo47 = 644597538;
const unsigned long IDNo48 = 645654050;
const unsigned long IDNo49 = 908125986;
const unsigned long IDNo50 = 870354471;
const unsigned long IDNo51 = 641960994;
const unsigned long IDNo52 = 864795175;
const unsigned long IDNo53 = 1924384036;
const unsigned long IDNo54 = 2485213214;
const unsigned long IDNo55 = 2461214497;
const unsigned long IDNo56 = 382526994;
const unsigned long IDNo57 = 3804710;
const unsigned long IDNo58 = 1171485483;
const unsigned long IDNo59 = 2219427870;
const unsigned long IDNo60 = 2475915556;
const unsigned long IDNo61 = 1172171819;
const unsigned long IDNo62 = 1668766247;
const unsigned long IDNo63 = 1402798887;
const unsigned long IDNo64 = 1663632679;
const unsigned long IDNo65 = 1671576103;
const unsigned long IDNo66 = 1408794919;
const unsigned long IDNo67 = 863999783;
const unsigned long IDNo68 = 1131704103;
const unsigned long IDNo69 = 906737442;
const unsigned long IDNo70 = 870631719;
const unsigned long IDNo71 = 1131134247;
const unsigned long IDNo72 = 642094882;
const unsigned long IDNo73 = 640544802;
const unsigned long IDNo74 = 864495655;
const unsigned long IDNo75 = 640638754 ;
const unsigned long IDNo76 = 653320738 ;
const unsigned long IDNo77 = 865896231 ;
const unsigned long IDNo78 = 1358432263 ;
const unsigned long IDNo79 = 1347086855 ;
const unsigned long IDNo80 = 1912395529 ;
const unsigned long IDNo81 = 1346890759 ;
const unsigned long IDNo82 = 1911782409 ;
const unsigned long IDNo83 = 1907722761 ;
const unsigned long IDNo84 = 1904493833 ;
const unsigned long IDNo85 = 1344639239 ;
const unsigned long IDNo86 = 1345546503 ;
const unsigned long IDNo87 = 1342913799 ;
const unsigned long IDNo88 = 1924713764 ;
const unsigned long IDNo89 = 1122658340 ;
const unsigned long IDNo90 = 642510370 ;
const unsigned long IDNo91 = 1916999204 ;
const unsigned long IDNo92 = 1118269476 ;
const unsigned long IDNo93 = 863412263 ;
const unsigned long IDNo94 = 1668822823 ;
const unsigned long IDNo95 = 1159891757 ;
const unsigned long IDNo96 = 1924134180 ;
const unsigned long IDNo97 = 1920996900 ;
const unsigned long IDNo98 = 903611949 ;
const unsigned long IDNo99 = 1432406573 ;
const unsigned long IDNo100 = 652700194 ;

String manualString = "";
bool completeString = false;

//1200942445 - 4,1870775848 - 1
//1863600424 -2  1396368686 - 3
//656470381 -5 930540397-6 661132654 - 8 1731786093 - 9 3072335214-10

unsigned long cardPoint[] = {IDNo22, IDNo4};
bool lightSafetySensor;
unsigned long upTimer = 0;
unsigned long leftTimer = 0;
unsigned long rightTimer = 0;
unsigned long buttonTimer = 0;

bool lightStatus = true, srfFlag = false;
int lightCount = 0;
unsigned long lightDelay = 0;



unsigned long pointSend = 99, sendTime = 0, informTime = 0 ;
bool pointTaskFlag = true;
String pointMess , responseMess; // "#______data____\n",

int event1 = EVENT_TURN;
bool button1low = false, button2low = false;

const unsigned long schedule1[] = {IDNo349,IDNo364};
const int schedule1Point = sizeof(schedule1)/sizeof(unsigned long);
const int schedule1Event[schedule1Point] = {FRONT_TURNLEFT_NORMAL,BACK_TURNLEFT_NORMAL};
const int schedule1Lift[schedule1Point] = {UP,DOWN};
int liftStatus = DOWN;
int recentSchedule1 = 0;
bool scheduleFlag = false;
int scheduleSpeed;
int event2 = EVENT_TURN;
const int schedule2Point = 2;
const unsigned long schedule2[schedule2Point] = {IDNo355,IDNo364};
const int schedule2Event[schedule2Point] = {EVENT_TURN,EVENT_TURN};

const unsigned long backCard[] = {IDNo69,IDNo226};
int backCardCount =  sizeof(backCard) / sizeof(unsigned long);

const unsigned long frontCard[] = { IDNo229};
int frontCardCount =  sizeof(frontCard) / sizeof(unsigned long);

bool brakeFlag = false, timerFlag = false;
bool lamp1Flag = false, lineTrack = true , EMGFlag = false, lampStatus1 = false, lampStatus2 = false, lampStatus3 = false, safetyFlag = false;

byte safetySensor = 0;
bool safetySFlag = 1, rfStopFlag = false, rfSlowFlag = false;
int safetyCount = 0, safetyCount1 = 0;
double safetyResult = 0, safetySum = 0;
unsigned long timeCount, bumpDelay = 0, safetyDelay = 0, holdSpeedTimer = 0;
int route;
bool a;
int currentIndex = 0;
class LightSensor {
  private:
    int lightCount = 0;
    unsigned long lightDelay = 0;
    long sensorValue = 0;
    int inputPin;
    int sensorScale;

  public:
    bool lightStatus = true;
    LightSensor(int _inputPin, int _sensorScale);
    bool getStatus();
    void getValue();
};

#define ADJBOOSTSPEED 9
#define ADJTURNSPEED 8
#define ADJNORMALSPEED 7

void speedModify() {
  NORMALSPEED += ADJNORMALSPEED;
  TURNSPEED += ADJTURNSPEED;
  BOOSTSPEED += ADJBOOSTSPEED;
}

#include <Wire.h>
#include <LiquidCrystal_I2C.h>
#define VOLTAGEPIN 1
#define CURRENTPIN 0
#define SCALE_VOLTAGE 0.03761111
#define OFFSET_ACS 512
#define SCALE_CURRENT 0.072727272
#define OVER_CURRENT 9999//8.00
#define LOW_VOLTAGE 22.00//22.00
#define OVERLOAD_COUNT 25
#define LOW_BATTERY_COUNT 100
#define LCD_REFRESH_TIME 500
LiquidCrystal_I2C lcd(0x27, 16, 2);
int firstActiveCount = 0;
double analogValue(byte times, int pin) {
  int readValue = 0;
  double value = 0;
  for (int i = 0; i < times; i++) {
    readValue += analogRead(pin);
  }
  value = (double)readValue / times;
  return value;
}
double voltagePrev[3], currentPrev[3];
int ACSValue = 0;
double currentValue = 0;
unsigned long readValueTimer = 0, sendValueTimer = 0;
double voltageValue = 0;
bool phaseDisplay = false, fristActive = true;
double getVoltage() {
  voltageValue =  analogValue(5, VOLTAGEPIN);
  voltageValue *= SCALE_VOLTAGE;
  double value = voltageValue;
  voltageValue = (voltageValue + voltagePrev[0] + voltagePrev[1] + voltagePrev[2]) / 4;
  voltagePrev[2] = voltagePrev[1];
  voltagePrev[1] = voltagePrev[0];
  voltagePrev[0] = voltageValue;
  return value;
}
double getCurrent() {
  ACSValue = (double)analogValue(5, CURRENTPIN);
  if (ACSValue < OFFSET_ACS - 1) {
    currentValue = (OFFSET_ACS - ACSValue) * SCALE_CURRENT;
  }
  else if (ACSValue > OFFSET_ACS + 1) {
    currentValue = (ACSValue - OFFSET_ACS) * SCALE_CURRENT;
  }
  else currentValue = 0;

  double value = currentValue;
  currentValue = (currentValue + currentPrev[0] + currentPrev[1] + currentPrev[2]) / 4;
  currentPrev[2] = currentPrev[1];
  currentPrev[1] = currentPrev[0];
  currentPrev[0] = currentValue;
  return value;
}
void lcdDisplay() {
  if (millis() - readValueTimer > LCD_REFRESH_TIME) {
    lcd.setCursor(2, 0);
    lcd.print(voltageValue);
    lcd.print("V");// pos 10;
    lcd.setCursor(2, 1);
    lcd.print(currentValue);
    lcd.print("A");// pos 10;
    readValueTimer = millis();
  }

  if (millis() - sendValueTimer > 100050 || (fristActive)) {
    char tempChar[10];
    int tempValue = voltageValue * 100;
    sprintf(tempChar, "<%d\n", tempValue);
    String tempString = tempChar;
    Serial.print(tempString);
    Serial1.print(tempString);
    tempValue = currentValue * 100;
    sprintf(tempChar, ">%d\n", tempValue);
    tempString = tempChar;
    Serial.print(tempString);
    Serial1.print(tempString);
    sendValueTimer = millis();
    fristActive = false;
  }
}
int overCurrentCount, lowVoltageCount;

void batteryCheck() {
  getCurrent();
  getVoltage();
  if (currentValue > OVER_CURRENT && overCurrentCount < 1000) {
    overCurrentCount++;
  }
  else if (currentValue <= OVER_CURRENT) overCurrentCount = 0;
  if (voltageValue < LOW_VOLTAGE && lowVoltageCount < 1000) {
    lowVoltageCount++;
  }
  else if (voltageValue >= LOW_VOLTAGE) lowVoltageCount = 0;
  //Serial.println(overCurrentCount);
  if (overCurrentCount > OVERLOAD_COUNT) {
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("QUA TAI: ");
    lcd.print(currentValue);
    lcd.print("A");
    lcd.setCursor(0, 1);
    lcd.print("KIEM TRA LAI AGV");
    analogWrite(LM, 0); analogWrite(RM, 0);
    stopCard = OVERLOAD;
    UART(stopCard);
    while (1);
  }
  while (lowVoltageCount > LOW_BATTERY_COUNT) {
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("HET PIN: ");
    lcd.print(voltageValue);
    lcd.print("V");
    lcd.setCursor(0, 1);
    lcd.print("CAN THAY THE PIN");
    analogWrite(LM, 0); analogWrite(RM, 0);
    stopCard = BATTERYDOWN;
    UART(stopCard);
    while (1);
  }
}
void setup()
{
  speedModify();
  pinMode(LIFT_REST,OUTPUT);
  digitalWrite(LIFT_REST,LOW);
  pinMode(EMG1, INPUT_PULLUP);
  pinMode(START1, INPUT_PULLUP);
  pinMode(START2, INPUT_PULLUP);
  pinMode(_RALM, INPUT);
  pinMode(RD, OUTPUT);
  pinMode(LD, OUTPUT);
  pinMode(_LALM, INPUT);
  pinMode(_LBRK, OUTPUT);
  pinMode(_LM, OUTPUT);
  pinMode(_RBRK, OUTPUT);
  pinMode(_RM, OUTPUT);

  pinMode(START3, INPUT_PULLUP);
  pinMode(START4, INPUT_PULLUP);
  pinMode(EMG2, INPUT_PULLUP);
  pinMode(LAMP1, OUTPUT);
  pinMode(LAMP2, OUTPUT);
  pinMode(LAMP3, OUTPUT);
  pinMode(LAMP4, OUTPUT);
  pinMode(SPK, OUTPUT);  
  pinMode(FRONT_RIGHT, OUTPUT);
  pinMode(FRONT_LEFT, OUTPUT);
  pinMode(BACK_RIGHT, OUTPUT);
  pinMode(BACK_LEFT, OUTPUT);
  pinMode(FRONT_SAFETY, INPUT_PULLUP);
  pinMode(FRONT_WARNING, INPUT_PULLUP);
  pinMode(BACK_WARNING, INPUT_PULLUP);
  pinMode(BACK_SAFETY, INPUT_PULLUP);
  pinMode(LIFT_PWM,OUTPUT);
  analogWrite(LIFT_PWM,0);
  pinMode(LIFT_DIR,OUTPUT);
  digitalWrite(LIFT_DIR,DOWN);
  lcd.init();
  lcd.backlight();
  lcd.setCursor(0, 0);
  lcd.print("FRONT_SET ...");
  frontSetup();
  delay(3000);
  digitalWrite(LIFT_DIR,UP);
  analogWrite(LIFT_PWM,100);
  digitalWrite(LIFT_REST,HIGH);
  lcd.setCursor(0, 1);
  lcd.print("LIFT_UP.....");
  currentLift = UP;
  delay(3000);
  digitalWrite(LIFT_REST,LOW);
  analogWrite(LIFT_PWM,0);
  delay(2000);
  lcd.setCursor(0, 1);
  lcd.print("LIFT_DOWN...");
  lift(DOWN); 
  lcd.clear();
  Serial.begin(74880);
  Serial1.begin(74880);
  SPI.begin();
  mfrc522.PCD_Init();


  myPID.SetMode(AUTOMATIC);
  myPID.SetOutputLimits(-255, 255);
  myPID.SetSampleTime(10);
  nameCall();


  lcd.setCursor(0, 0);
  lcd.print("U:");
  lcd.setCursor(0, 1);
  lcd.print("I:");
  
  

  voltagePrev[0] = getVoltage();
  voltagePrev[1] = voltagePrev[0];
  voltagePrev[2] = voltagePrev[0];

  currentPrev[0] = getCurrent();
  currentPrev[1] = currentPrev[0];
  currentPrev[2] = currentPrev[0];
  Serial1.print('\n');
  PIDInit();
}
/*********** HAM DIEU KHIEN MOTOR **************/

/********** HAM DUNG DONG CO ***********/
void StopAGV()
{
  smaxr = 0;
  smaxl = 0;
  analogWrite(RM, 0);
  analogWrite(LM, 0);
  digitalWrite(RBRK, HIGH);
  digitalWrite(LBRK, HIGH);

}
/*********** HAM DOC THE RFID *************/
void RFIDread()
{
  // Searching new card
  if ( ! mfrc522.PICC_IsNewCardPresent())
  {
    return;
  }
  // Reading card
  if ( ! mfrc522.PICC_ReadCardSerial())
  {
    return;
  }
  CardID = 0;

  // Show Card ID
  for (byte i = 0; i < mfrc522.uid.size; i++)
  {
    CardIDTemp = mfrc522.uid.uidByte[i];
    CardID = CardID * 256 + CardIDTemp;
  }
  CardID = replaceCard(CardID);
  Serial.println(CardID);
  mfrc522.PICC_HaltA();
  mfrc522.PCD_StopCrypto1();
  lcd.setCursor(13, 0);
  lcd.print(CardID);
  if ((lampStatus1 && checkCardMSKTr(CardID)) || (lampStatus2 && checkCardMSIOT(CardID))) {

    char mystr[40];
    pointMess = "";
    sprintf(mystr, "#%lu\n", 10001);
    pointMess += mystr;
    Serial1.print(pointMess);

    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("AGV NHAM DUONG");
    lcd.setCursor(0, 1);
    lcd.print(CardID);

    StopAGV();
    while (1);
  }
  /*for (int i = 0; i < frontCardCount; i++) {
    if (CardID == frontCard[i] && !frontSelect) {
      if(!checkSwitchFlag2 && !checkSwitchFlag1)switchFrontFlag = true;
      Serial.println("Front Card");
      return;
    }
  }
  for (int i = 0; i < backCardCount; i++) {
    if (CardID == backCard[i] && !backSelect) {
      if(!checkSwitchFlag2 && !checkSwitchFlag1)switchBackFlag = true;
      Serial.println("Back Card");
      return;
    }
  }*/
}
//1200942445 - 4,1870775848 - 1
//1863600424 -2  1396368686 - 3
//656470381 -5 930540397-6 661132654 - 8 1731786093 - 9 3072335214-10


/*********** MAIN ***************/
void loop()
{

  //updateUART();
  getInput();
  //checkSwitch();
  analyst();
  batteryCheck();
  lcdDisplay();
  if (rightALM == LOW || leftALM == LOW) {
    stopCard = OVERLOAD;
    UART(stopCard);
    while (digitalRead(RALM) == LOW || digitalRead(LALM) == LOW) {
      analogWrite(LM, 0); analogWrite(RM, 0);
      //digitalWrite(LAMP1,!digitalRead(LAMP1));
      //delay(1000);
    }
  }
  UART();

  /* //Serial.println( ss1.getTotalStt());
    if (digitalRead(EMG)) mode = AUTO;
    else mode = MAN;
    switch (mode)
    {
    case AUTO:
      if ( a = ((digitalRead(EMG) == 1) && ( ss1.getTotalStt() || analogRead(A1) < 800 || digitalRead(BUMP) == HIGH)) && digitalRead(LAMP1) == HIGH )
      {
        if ( ss1.getTotalStt() == 1 && analogRead(A1) > 800  && digitalRead(BUMP) != HIGH && digitalRead(LAMP1) == HIGH)
        {
          Speedcontrol2();

        }
        else
        {
          StopAGV();

        }
      }
      else
      {
        Speedcontrol1();
        //Serial.println( " b");
      }
      break;
    case MAN:
    //Serial.println( " a");
      StopAGV();
      while(digitalRead(START2)){analogWrite(RM,60);analogWrite(LM,30);digitalWrite(RBRK, LOW);digitalWrite(LBRK, LOW);}
      while(digitalRead(START3)){analogWrite(RM,30);analogWrite(LM,60);digitalWrite(RBRK, LOW);digitalWrite(LBRK, LOW);}
     // while(digitalRead(START1)){analogWrite(RM,100);analogWrite(LM,100);digitalWrite(RBRK, LOW);digitalWrite(LBRK, LOW);}
      digitalWrite(LAMP1, LOW);
      break;
    }

    //Serial.println(analogRead(A0));*/
}

void turnRight() {
  digitalWrite(FORK_L, HIGH);
  digitalWrite(FORK_R, LOW);
}
void turnLeft() {
  digitalWrite(FORK_L, LOW);
  digitalWrite(FORK_R, HIGH);
}
void noTurn() {
  digitalWrite(FORK_L, LOW);
  digitalWrite(FORK_R, LOW);
}
void settingSpeed(int a) {
  smaxr = a;
  smaxl = a;
}
void softStop(int x) {
  if (smaxr > smaxl) smaxr = smaxl;
  else smaxl = smaxr;
  if (( smaxr  >= x || smaxl  >= x ) && millis() - timeCount > 10 ) {
    if (smaxr >= x) smaxr -= x;
    if (smaxl >= x) smaxl -= x;
    timeCount = millis();
  }
  else if ( smaxr  < x && smaxl  < x ) {
    StopAGV();
  }
}
void softStart(int setPoint) {
  if (setPoint == 0) return;
  digitalWrite(RBRK, LOW);
  digitalWrite(LBRK, LOW);
  smaxr = setPoint + setPoint * RIGHTADD / 100;
  smaxl = setPoint + setPoint * LEFTADD / 100;

  motor(Output);


}
void getInput () {

  //if (checkButton(START1,HIGH) && digitalRead(LAMP1)!=HIGH && lamp1Flag == false) {openLamp(LAMP1);}
  if (checkButton(START1, LOW)  ) {
    if (lampStatus1 == false && button1low == true) {
      openLamp(LAMP1);
      lcd.setCursor(11, 1);
      lcd.print("NUT1");
      button1low = false;
    }
    else if (lampStatus1 == true && button1low == true) {
      closeLamp();
      button1low = false;
    }
  }
  else if (checkButton(START2, LOW)) {
    if (lampStatus2 == false && button2low == true) {
      openLamp(LAMP2);
      lcd.setCursor(11, 1);
      lcd.print("NUT2");
      button2low = false;
    }
    else if (lampStatus2 == true && button2low == true) {
      closeLamp();
      button2low = false;
    }
  }
  else {
    if (digitalRead(START1) == HIGH) {
      button1low = true;
    }
    if (digitalRead(START2) == HIGH) {
      button2low = true;
    }
  }
  //Serial.println(button1low);
  //else if (checkButton(START3,HIGH)) {openLamp(LAMP3);}
  //else if (digitalRead(START1) == LOW && digitalRead(LAMP1)==HIGH ) lamp1Flag = true;
  //else if (checkButton(START1,HIGH) && digitalRead(LAMP1)==HIGH && lamp1Flag == true) { closeLamp();}
  //else if (digitalRead(START1) == LOW && digitalRead(LAMP1)==LOW ) {lamp1Flag = false;}



  //rfStopFlag = digitalRead(RFSTOP);
  if (digitalRead(WARNING) == LOW) setSlowSpeed();
  else setDefaultSpeed();

  if (!checkButton(EMG1, HIGH) && !checkButton(EMG2, HIGH)) EMGFlag = false; // EMG safe
  else {
    closeLamp(); EMGFlag = true;
  } // EMG unsafe

  if (getLine()) {
    lineTrack = false;
    closeLamp();
  }
  else lineTrack = true;

  pos = analogRead(LINEANALOG);
  Input = inputScale(pos);
  myPID.Compute();

  //checkSafetySensor();
  rfidConnectTime = millis();
  RFIDread();

  if (millis() - rfidConnectTime > 100) {
    rfidConnectTime = millis() - rfidConnectTime;

    lcd.setCursor(8, 1);
    lcd.print(rfidConnectTime);

    SPI.end();
    delay(1);
    SPI.begin();

    mfrc522.PCD_Init();

  }

  safetyFlag = !EMGFlag  && lineTrack && digitalRead(SAFETY);  //safetyFlag =    1 - Safe       0 - Unsafe

  //speedModify();

  leftALM = digitalRead(LALM);
  rightALM = digitalRead(RALM);
}
void openLamp(int Lamp) {

  if (Lamp == LAMP1) {
    digitalWrite(LAMP1, HIGH);
    digitalWrite(LAMP2, LOW);
    digitalWrite(SPK, HIGH);
    prevCard = 1000;
    lampStatus1 = true;
    lampStatus2 = false;

  }
  else if (Lamp == LAMP2) {
    digitalWrite(LAMP1, LOW);
    digitalWrite(LAMP2, HIGH);
    digitalWrite(SPK, HIGH);
    prevCard = 1000;
    lampStatus1 = false;
    lampStatus2 = true;
  }

}
void closeLamp() {
  //Serial.println(closeStatus);
  ///EEPROM.update(99,closeStatus);
  digitalWrite(LAMP1, LOW);
  digitalWrite(LAMP2, LOW);
  digitalWrite(SPK, LOW);
  lampStatus1 = false;
  lampStatus2 = false;


}
void brakeActive() {
  digitalWrite(RBRK, HIGH); digitalWrite(LBRK, HIGH);
}
void brakeDeactive() {
  digitalWrite(RBRK, LOW); digitalWrite(LBRK, LOW);
}
void stopMotor () {
  analogWrite(RM, 0); analogWrite(LM, 0);
}
void analyst() {
  if (EMGFlag) {
    softStop(100);
    stopCard = EMGSTOP;
    UART(stopCard);
    brakeActive();
    brakeFlag = true;
    while (EMGFlag) {
      EMGFlag = digitalRead(EMG1) && digitalRead(EMG2);
      while (!digitalRead(START2)) {
        analogWrite(RM, 60);
        analogWrite(LM, 30);
        digitalWrite(RBRK, LOW);
        digitalWrite(LBRK, LOW);
        smaxr = 50;
        smaxl = 50;
        while (!digitalRead(START1) && !digitalRead(START2)) {
          analogWrite(RM, 70 );
          analogWrite(LM, 70);
          digitalWrite(RBRK, LOW);
          digitalWrite(LBRK, LOW);
          smaxr = 100;
          smaxl = 100;
        }
      }

      while (!digitalRead(START1)) {
        analogWrite(RM, 30);
        analogWrite(LM, 60);
        digitalWrite(RBRK, LOW);
        digitalWrite(LBRK, LOW);
        smaxr = 50;
        smaxl = 50;
      }
      //checkManual();
      if (brakeFlag == true) brakeActive();
      else  brakeDeactive();
      stopMotor ();
      smaxr = 0; smaxl = 0;
    }
  }



  else if (lampStatus1 && safetyFlag &&  !rfStopFlag) {
    stopCard = KHOTRONG;
    UART(stopCard);
    event1 = checkSchedule1(); //Serial.println("2 status");
    eventOutput(event1);//scheduleSpeed
    softStart(scheduleSpeed);
  }
  else if (lampStatus2 && safetyFlag &&  !rfStopFlag) {
    stopCard = IOT3;
    UART(stopCard);
    event2 = checkSchedule2();
    eventOutput(event2);//scheduleSpeed
    softStart(scheduleSpeed);
  }
  else if (((!lampStatus1 && !lampStatus2) || rfStopFlag) && safetyFlag) { // || rfStopFlag

    holdSpeed(0);
    softStart(scheduleSpeed);
    if (!lampStatus1 && !lampStatus2) {
      stopCard = PRESSSTOP;
      UART(stopCard);
    }
    else if (rfStopFlag) {
      stopCard = AVOIDSTOP;
      UART(stopCard);
    }
  }
  else {
    //closeLamp();
    softStop(30);
    stopCard = UNKNOWSTOP;
    if (safetySensor == 2) stopCard = SAFETYSTOP;
    else if (!lineTrack) stopCard = LINETRACKSTOP;
    UART(stopCard);
  }

}
int checkSchedule1() {
  if (prevCard != CardID) {
    prevCard = CardID;
    pointTaskFlag = true;
    if(CardID == schedule1[currentIndex]){
      int result = schedule1Event[currentIndex];
      currentIndex++;    
      if(currentIndex >= schedule1Point) currentIndex = 0;
      lcd.setCursor(8,0);
      lcd.print("  ");
      lcd.setCursor(8,0);
      lcd.print(currentIndex);
      return result;
    }
    else return event1;
    /*for (int i = 0 ; i < schedule1Point; i++ ) {
      if ( CardID == schedule1[i] ) {
        //if (schedule1Event[i] == EVENT_STOP && (event1 == EVENT_STOP )){return NO_EVENT;}
        return schedule1Event[i];
      }
    }
    return NO_EVENT;*/
  }
  return event1;
}
int checkSchedule2() {
  if (prevCard != CardID) {
    prevCard = CardID;
    pointTaskFlag = true;
    for (int i = 0 ; i < schedule2Point; i++ ) {
      if ( CardID == schedule2[i] ) {
        //if (schedule2Event[i] == EVENT_STOP && (event2 == EVENT_STOP )){return NO_EVENT;}
        StopAGV();
        delay(30000);
        return schedule2Event[i];
      }
    }
    return NO_EVENT;
  }
  return event2;
}
/*
      >>>>>>>>>>>>> (A1)123728749 >>>>>>>>>>>>>>>>>>>>>>>>(B1) 1070081065 >>>>>>>>>>>>>>>>>>(C1) 4158125421 >>>>>>>>>>>>>>>>>(D1) 1607974440 >>>>>
      <<<<<(A2) 2400236073 <<<<<<<<<<<<<<<(B2) 1329283881 <<<<<<<<<<<<<<<<<<<<<<<(C2) 3884446573<<<<<<<<<<<<<<<<<<<(D2) 666823789 <<<<<<<<<<<<<<<<
*/
void eventOutput(int evt) {
  int liftIndex;
  switch (evt) {
    case  EVENT_TURNLEFT_IN:
      holdSpeed(TURNSPEED);
      turnLeft();
      break;

    case  EVENT_TURNLEFT_IN1:
      holdSpeed(SLOWSPEED);
      turnLeft();
      break;

    case EVENT_TURNLEFT_OUT:
      scheduleSpeed = NORMALSPEED;
      noTurn();
      break;

    case EVENT_TURNRIGHT_IN:
      holdSpeed(TURNSPEED);
      turnRight();
      break;
    case EVENT_TURNRIGHT_IN1:
      holdSpeed(SLOWSPEED);
      turnRight();
      break;
    case EVENT_TURNLEFT_IN2:
      holdSpeed(BOOSTSPEED);
      turnLeft();
      break;
    case EVENT_TURNRIGHT_IN2:
      holdSpeed(BOOSTSPEED);
      turnRight();
      break;
    case EVENT_TURNLEFT_IN3:
      holdSpeed(NORMALSPEED);
      turnLeft();
      break;
    case EVENT_TURNRIGHT_IN3:
      holdSpeed(NORMALSPEED);
      turnRight();
      break;
    case FRONT_TURNRIGHT_NORMAL:
      if(frontSelect != true){
        StopAGV();
        delay(500);
        frontSetup();
        delay(200);
      }
      liftIndex = currentIndex-1;
      if(liftIndex < 0) liftIndex = schedule1Point-1;
      if(currentLift != schedule1Lift[liftIndex]){
        lcd.setCursor(10,0);
        lcd.print("  ");
        lcd.setCursor(10,0);
        lcd.print(liftIndex);
        StopAGV();
        delay(500);
        lift(schedule1Lift[liftIndex]);
        delay(3000);
      }
        
      holdSpeed(NORMALSPEED);
      turnRight();
      break;
    case FRONT_TURNLEFT_NORMAL:
      if(frontSelect != true){
        StopAGV();
        delay(500);
        frontSetup();
        delay(200);
      }
      liftIndex = currentIndex-1;
      if(liftIndex < 0) liftIndex = schedule1Point-1;
      if(currentLift != schedule1Lift[liftIndex]){
        lcd.setCursor(10,0);
        lcd.print("  ");
        lcd.setCursor(10,0);
        lcd.print(liftIndex);
        StopAGV();
        delay(500);
        lift(schedule1Lift[liftIndex]);
        delay(3000);
      }
        
      holdSpeed(NORMALSPEED);
      turnLeft();
      break;
    case BACK_TURNRIGHT_NORMAL:
      if(backSelect != true){
        StopAGV();
        delay(500);
        backSetup();
        delay(200);
      }
      liftIndex = currentIndex-1;
      if(liftIndex < 0) liftIndex = schedule1Point-1;
      if(currentLift != schedule1Lift[liftIndex]){
        lcd.setCursor(10,0);
        lcd.print("  ");
        lcd.setCursor(10,0);
        lcd.print(liftIndex);
        StopAGV();
        delay(500);
        lift(schedule1Lift[liftIndex]);
        delay(3000);
      }
        
      holdSpeed(NORMALSPEED);
      turnRight();
      break;
    case BACK_TURNLEFT_NORMAL:
      if(backSelect != true){
        StopAGV();
        delay(500);
        backSetup();
        delay(200);
      }
      liftIndex = currentIndex-1;
      if(liftIndex < 0) liftIndex = schedule1Point-1;
      if(currentLift != schedule1Lift[liftIndex]){
        lcd.setCursor(10,0);
        lcd.print("  ");
        lcd.setCursor(10,0);
        lcd.print(liftIndex);
        StopAGV();
        delay(500);
        lift(schedule1Lift[liftIndex]);
        delay(3000);
      }
        
      holdSpeed(NORMALSPEED);
      turnLeft();
      break;
    
    case EVENT_TURNRIGHT_OUT:
      holdSpeed(NORMALSPEED);
      noTurn();
      break;

    case EVENT_STOP:
      scheduleSpeed = TURNSPEED;
      CardID = 200;
      //closeStatus = 5;
      closeLamp();
      noTurn();
      liftIndex = currentIndex-1;
      if(currentIndex < 0) break;
      if(currentLift != schedule1Lift[liftIndex]){
        StopAGV();
        delay(500);
        lift(schedule1Lift[liftIndex]);
        delay(3000);
      }
      event1 = NO_EVENT;
      event2 = NO_EVENT;
      break;
    case EVENT_BOOST:
      holdSpeed(BOOSTSPEED);
      noTurn();
      break;
    case EVENT_NORMAL:
      holdSpeed(NORMALSPEED);
      noTurn();
      break;
    case EVENT_SLOW:
      holdSpeed(SLOWSPEED);
      noTurn();
      break;
    case EVENT_TURN:
      holdSpeed(TURNSPEED);
      noTurn();
      break;
    default:
    case NO_EVENT:
      if (lampStatus1 == true) holdSpeed(TURNSPEED);
      else if (lampStatus2 == true) holdSpeed(NORMALSPEED);
      else holdSpeed(TURNSPEED);
      turnLeft();
      break;
  }
}

int inputScale(int input) {


  return input;
}
void motor (int output) {
  int speedR = smaxr, speedL = smaxl;
  output *= 10;
  if (output < 0) speedR -= map(output * -1, 0, 2550, 0, 100) * speedR / 100;
  if (output > 0) speedL -= map(output, 0, 2550, 0, 100) * speedL / 100;


  analogWrite(RM, speedR);
  analogWrite(LM, speedL);
  //Serial.println(speedR);
  //Serial.println(speedL);
}
void nameCall() {
  String nameCall;
  switch (NAME) {
    case 1:
      nameCall = "CAT BA";
      break;
    case 2:
      nameCall = "HA LONG";
      break;
    case 3:
      nameCall = "BAI CHAY";
      break;
    case 4:
      nameCall = "DO SON";
      break;
    case 5:
      nameCall = "TRA CO";
      break;
    case 6:
      nameCall = "SA PA";
      break;
    case 7:
      nameCall = "HA GIANG";
      break;
    case 8:
      nameCall = "QUAT LAM";
      break;
    default:
      nameCall = (String)NAME;
      break;
  }
  Serial.print("AGV NAME: " );
  Serial.println(nameCall);
  Serial.print("RIGHT ADD : " );
  Serial.print(RIGHTADD);
  Serial.print("%" );
  Serial.print("    --    LEFT ADD : " );
  Serial.print(LEFTADD);
  Serial.println("%" );

}
void holdSpeed(int x) {
  if (millis() - holdSpeedTimer > 30) {
    if (smaxr >= smaxl) currentSpeed = smaxl ; // lay gia tri toc do hien tai
    else currentSpeed = smaxr ;
    prevSpeed = currentSpeed  ;

    if (currentSpeed > x ) {
      currentSpeed -= (0.8 + pointValue);  // giam gia tri
      pointValue = prevSpeed - currentSpeed;
    }
    else if (currentSpeed == x) {
      currentSpeed = x;  // giu nguyen
      pointValue = 0;
    }
    else if (currentSpeed < x && currentSpeed >= BEGINSPEED) {
      currentSpeed += (0.12 + pointValue);  //  tang gia tri
      pointValue = currentSpeed - prevSpeed;
    }
    else {
      currentSpeed = BEGINSPEED;  // start speed
      pointValue = 0;
    }
    if (x == 0 && currentSpeed < 20) {
      StopAGV();  // che do dung khi dat gia tri
      pointValue = 0;
      currentSpeed = 0;
    }
    if (pointValue >= 1.0) pointValue -= 1;
    scheduleSpeed = currentSpeed;
    holdSpeedTimer = millis();
  }

}
bool   getLine() {
  for (int i = 1; i <= 500; i++) {
    if (analogRead(LINETRACK) > 750) return 0;
    else delay(2);
  }
  return 1;
}
bool   checkButton(byte button, bool logic) {
  for (int i = 1; i <= 4; i++) {
    if (digitalRead(button) != logic) return 0;
    else delayMicroseconds(100);
  }
  return 1;
}
/*void checkSafetySensor(){
  bool checkSensor = digitalRead(SAFETY);



  if (safetySFlag){

    if (checkSensor == HIGH) {safetySensor= LOW;safetyCount = 0;safetySFlag= true ;return;}
    else safetyCount++;
    if (safetyCount > 10) {safetySensor= HIGH;safetyCount = 0;safetyCount1 = 0;safetySum=0;safetyResult=0;safetySFlag= false ;return;}

    }
  else {

    safetyCount++ ;
    safetySum += checkSensor ;
    safetyResult = safetySum/safetyCount;
    if (safetyCount >= 30 ) {
      if (safetyResult > 0.3) { safetyCount1 ++;}
      else {safetyCount1 = 0;safetyDelay = millis();}
      safetyCount = 0;
      safetySum=0;
      safetyResult=0;
      }

    if (safetyCount1 > 4 && millis() - safetyDelay >5000) {safetySensor= LOW;safetyCount = 0;safetySFlag= true ;return;}
    }


  }
*/
void UART() {

  if (!pointTaskFlag) return;
  pointMess = "";
  char mystr[40];
  sprintf(mystr, "#%lu\n", prevCard);
  pointMess += mystr;
  Serial.print(pointMess);
  Serial1.print(pointMess);
  sendTime = micros();

  pointTaskFlag = false;
  return;
}
void UART(unsigned long stopCard) {

  if ( stopCard == prevStopCard) return;
  pointMess = "";
  char mystr[40];
  sprintf(mystr, "#%lu\n", stopCard);
  pointMess += mystr;
  Serial.print(pointMess);
  Serial1.print(pointMess);
  sendTime = micros();
  prevStopCard = stopCard;

  return;
}
void recallAutoDoor(unsigned long stopCard) {
  pointMess = "";
  char mystr[40];
  sprintf(mystr, "&%lu\n", stopCard);
  pointMess += mystr;
  Serial.print(pointMess);
  Serial1.print(pointMess);
}
void checkManual() {
  do {
    while (Serial1.available()) {
      char inChar = (char)Serial1.read();
      manualString += inChar;
      if (inChar == '\n') {
        if (manualString.substring(0, 2) == "UP") {
          upTimer = millis();
          buttonTimer = millis();
        }
        else if (manualString.substring(0, 2)  == "RI") {
          rightTimer = millis();
          buttonTimer = millis();
        }
        else if (manualString.substring(0, 2)  == "LE") {
          leftTimer = millis();
          buttonTimer = millis();
        }
        else if (manualString.substring(0, 2)  == "ST") {
          upTimer = 0 ;
          leftTimer = 0 ;
          rightTimer = 0;
          buttonTimer = 0;
          brakeFlag = true;
        }
        else if (manualString.substring(0, 2)  == "FL") {
          upTimer = 0 ;
          leftTimer = 0 ;
          rightTimer = 0;
          buttonTimer = 0;
          brakeFlag = false;
        }
        manualString = "";
      }
    }
    if (millis() - upTimer < 200) {
      analogWrite(RM, 100);
      analogWrite(LM, 100);
      digitalWrite(RBRK, LOW);
      digitalWrite(LBRK, LOW);
      smaxr = 100;
      smaxl = 100;
    }
    else if (millis() - leftTimer < 200) {
      analogWrite(RM, 80);
      analogWrite(LM, 50);
      digitalWrite(RBRK, LOW);
      digitalWrite(LBRK, LOW);
      smaxr = 50;
      smaxl = 50;
    }
    else if (millis() - rightTimer < 200) {
      analogWrite(RM, 50);
      analogWrite(LM, 80);
      digitalWrite(RBRK, LOW);
      digitalWrite(LBRK, LOW);
      smaxr = 50;
      smaxl = 50;
    }
    else {
      analogWrite(RM, 0);
      analogWrite(LM, 0);
    }
  }
  while (millis() - buttonTimer < 1000);
}
LightSensor::LightSensor(int _inputPin, int _sensorScale) {
  inputPin = _inputPin;
  sensorScale = _sensorScale;
}
bool LightSensor::getStatus() {
  sensorValue = analogRead(inputPin) + sensorScale;
  if (sensorValue > 0/*LIGHTRANGE*/ ) {
    if (lightCount < 10)  lightCount++;
  }
  else lightCount = 0;
  if (lightCount > 3) {
    lightStatus = false ;
    lightDelay = millis();
  }
  else if (lightCount <= 3 && millis() - lightDelay > 5000) {
    lightStatus =  true;
  }
  return lightStatus;
}
void LightSensor::getValue() {
  Serial.print(lightStatus);
  Serial.print("___");
  Serial.println(sensorValue);
}
void frontSetup() {
  LINETRACK = FRONT_LINE_DETECT;
  LINEANALOG = FRONT_LINE_ANALOG;
  FORK_L = FRONT_LEFT;
  FORK_R = FRONT_RIGHT;

  RM = _LM;
  RBRK = _LBRK;
  RALM = _LALM;
  LM = _RM;
  LBRK = _RBRK;
  LALM = _RALM;
  digitalWrite(RD, HIGH);
  digitalWrite(LD, LOW);

  SAFETY = FRONT_SAFETY;
  WARNING = FRONT_WARNING;

  frontSelect = true;
  backSelect = false;
}
void backSetup() {
  LINETRACK = BACK_LINE_DETECT;
  LINEANALOG = BACK_LINE_ANALOG;
  FORK_L = BACK_LEFT;
  FORK_R = BACK_RIGHT;

  RM = _RM;
  RBRK = _RBRK;
  RALM = _RALM;
  LM = _LM;
  LBRK = _LBRK;
  LALM = _LALM;
  digitalWrite(LD, HIGH);
  digitalWrite(RD, LOW);

  SAFETY = BACK_SAFETY;
  WARNING = BACK_WARNING;

  frontSelect = false;
  backSelect = true;
}

void setDefaultSpeed() {
  NORMALSPEED = _NORMAL;
  DEFAULTSPEED = _DEFAULT;
  TURNSPEED = _TURN;
  BOOSTSPEED = _BOOST;
  SLOWSPEED = _SLOW;
}
void setSlowSpeed() {
  NORMALSPEED = _SLOW;
  DEFAULTSPEED = _SLOW;
  TURNSPEED = _SLOW;
  BOOSTSPEED = _SLOW;
  SLOWSPEED = _SLOW;
}
void liftUp(){
  digitalWrite(LIFT_DIR,UP);
  delay(500);
  for(int i = 30;i <= 180;i=i+2){
    analogWrite(LIFT_PWM,i);
    delay(10);
  }
  for(int i = 180;i >= 30;i=i-2){
    analogWrite(LIFT_PWM,i);
    delay(10);
  }
  digitalWrite(LIFT_REST,LOW);
  currentLift = UP;
}
void liftDown(){
  
  digitalWrite(LIFT_DIR,DOWN);
  delay(500);
  digitalWrite(LIFT_REST,HIGH);
  for(int i = 30;i <= 180;i=i+2){
    analogWrite(LIFT_PWM,i);
    delay(10);
  }
  for(int i = 180;i >= 30;i=i-2){
    analogWrite(LIFT_PWM,i);
    delay(10);
  }
  digitalWrite(LIFT_REST,LOW);
  currentLift = DOWN;
}
void lift(int dir){
  digitalWrite(LIFT_DIR,dir);
  delay(1000);
  digitalWrite(LIFT_REST,HIGH);
  for(int i = 0;i <= 150;i++){
    analogWrite(LIFT_PWM,i);
    delay(5);
  }
  delay(1000);
  for(int i = 150;i >= 40;i--){
    analogWrite(LIFT_PWM,i);
    delay(5);
  }
  delay(4000);
  digitalWrite(LIFT_REST,LOW);
  currentLift = dir;
}
void resetLift(){
  
}
