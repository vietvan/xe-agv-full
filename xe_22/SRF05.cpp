#include "Arduino.h"
#include "SRF05.h"

#define LV1 72
#define LV2 72


SRF05::SRF05(byte _triggerPin,byte _echoPin){
    echoPin = _echoPin;
    triggerPin = _triggerPin;
    pinMode(_echoPin, INPUT_PULLUP);
    pinMode(_triggerPin, OUTPUT);
    lv0 = 0;
    lv1 = 0;
    lv2 = 0;
    stt = 0;
    t = 1500;
    waveDelay = 100;
    objQty++;
    objNum= objQty -1;
  }
  
int SRF05::objQty = 0;
int SRF05::objStt[] = {0,0,0,0,0,0,0,0,0,0};
int SRF05::totalStt = 0;

int SRF05::getDistance(){
     
    digitalWrite(triggerPin, LOW);
    delayMicroseconds(2);
    digitalWrite(triggerPin, HIGH);
    delayMicroseconds(10);
    digitalWrite(triggerPin, LOW);

    duration= pulseIn(echoPin, HIGH,5000); // max 120cm
    
    return distance= (int)(((double)duration/29.1)/2.0);
    
    }
void SRF05::caculate(){
    if(millis() - waveDelay >= 5){
      getDistance();
      //Serial.println(distance );
    if( lv0+lv1+lv2 == 0 )
    {
      if (distance == 0 || distance >LV1) lv0++;
      else if (distance > 0 && distance < LV2) lv2++;
      else lv1++;
      return;
      }
   else if(distance == 0 || distance >LV2) {lv0++; lv1 = 0 ;lv2 = 0;}
   else if(distance > 0 && distance <=LV2) {lv2++; lv0 = 0 ;lv1 = 0;}
   else    {lv1++; lv2 = 0 ;lv0 = 0;}
   
   if (lv2 > 1 && (stt == 1 || stt == 0)) {t = millis(); stt = 2;objStt[objNum] = stt; }
   else if (lv0 >= 70 && ((stt == 1 && millis() - t > 4000) || (stt == 2 && millis() - t > 4000)))  {stt = 0;objStt[objNum] = stt;}
   else if ((lv1 >= 70 ) && (stt == 0 || (stt == 2 && millis() - t > 4000))) {t = millis(); stt = 1; objStt[objNum] = stt;}
   else {waveDelay = millis();return;}
   bool sttFlag = false;
   for(int i = 0 ; i < objQty ; i++) {
    if ( objStt[i] > 0){
       totalStt = 2;
       sttFlag = true;
       break;
    }        
   }
   if (!sttFlag)  totalStt = 0;  
   waveDelay = millis();
  }
}

int SRF05::getStatus(){
  return stt;
}
int SRF05::getObjNum(){
  return objNum;
}
int SRF05::getTotalStt(){
  return totalStt;
}


