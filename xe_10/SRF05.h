#ifndef SRF05_h
#define SRF05_h

#include "Arduino.h"

class SRF05 {
  private: 
    int distance;
    unsigned long duration,t;
    byte echoPin,triggerPin;
    int lv0,lv1,lv2,stt,objNum,LV1,LV2;
    unsigned long waveDelay;
    
    
    
  
  public:
    static int totalStt ,objStt[10],objQty ;
    SRF05(byte _triggerPin,byte _echoPin);
    int getDistance();
    void caculate();
    void SRF05::updateRangeMax();
    void SRF05::updateRangeMin();
    int getStatus();
    int getObjNum();
    int getTotalStt();
  };
#endif
