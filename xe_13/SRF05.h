#ifndef SRF05_h
#define SRF05_h

#include "Arduino.h"

class SRF05 {
  private: 
    int distance;
    unsigned long duration,t;
    byte echoPin,triggerPin;
    int lv0,lv1,lv2,stt,objNum;
    unsigned long waveDelay;
    
    
    
  
  public:
    static int totalStt ,objStt[10],objQty ;
    SRF05(byte _triggerPin,byte _echoPin);
    int getDistance();
    void caculate();
    int getStatus();
    int getObjNum();
    int getTotalStt();
  };
#endif
