#ifndef Header_h
#define Header_h
#define EEPROM_COUNT_ROUND 20
#define DIGITAL_LIGHTSENSOR_PIN 34
#define DIGITAL_LIGHTSENSOR_PIN2 33
#define DIGITAL_LIGHTSENSOR_COUNT 4
#define DIGITAL_LIGHTSENSOR_RESETTIME 5000

#define EPPROM_P 10
#define EPPROM_I 14
#define EPPROM_D 18
#define EVENT_TEMP_STOP 5
#define EVENT_TEMP_STOP_START 6
#define EVENT_TEMP_STOP_DESTINATION 7

#define TEMP_STOP_DELAY 8000
int digitalLightSensorCount = 0;
unsigned long digitalLightSensorTimer = 0,rfidConnectTime = 0;
bool digitalLightSensorActive = false;
String status;

#define _IDNo1 1345708295
#define _IDNo2 2169183753
#define _IDNo3 183269376
#define _IDNo4 1671576103
#define _IDNo5 1917949220
#define _IDNo6 1344967687
#define _IDNo7 1343335687
#define _IDNo8 1351433991
#define _IDNo9 1344585479
#define _IDNo10 1350706183
#define _IDNo11 867234087
#define _IDNo12 870565415
#define _IDNo13 865235751
#define _IDNo14 851230244
#define _IDNo15 1660064036
#define _IDNo16 866554151
#define _IDNo17 872260647
#define _IDNo18 865552935
#define _IDNo19 1126539815
#define _IDNo20 869715495
#define _IDNo21 865366567
#define _IDNo22 1129932327
#define _IDNo23 1126712359
#define _IDNo24 864580135
#define _IDNo25 867848231
#define _IDNo26 1127835431
#define _IDNo27 865686055
#define _IDNo28 1128798247
#define _IDNo29 868724263
#define _IDNo30 869454119
#define _IDNo31 1404991527
#define _IDNo32 1407677223
#define _IDNo33 904892205
#define _IDNo34 1404290087
#define _IDNo35 1124592167
#define _IDNo36 909560866
#define _IDNo37 1917504292
#define _IDNo38 1129588263
#define _IDNo39 1125363751
#define _IDNo40 2378078765
#define _IDNo41 1396368686
#define _IDNo42 112218898
#define _IDNo43 2691771556
#define _IDNo44 1129837351
#define _IDNo45 644198946
#define _IDNo46 649376290
#define _IDNo47 644597538
#define _IDNo48 645654050
#define _IDNo49 908125986
#define _IDNo50 870354471
#define _IDNo51 641960994
#define _IDNo52 864795175
#define _IDNo53 1924384036
#define _IDNo54 2485213214
#define _IDNo55 2461214497
#define _IDNo56 382526994
#define _IDNo57 3804710
#define _IDNo58 1171485483
#define _IDNo59 2219427870
#define _IDNo60 2475915556
#define _IDNo61 1172171819
#define _IDNo62 1668766247
#define _IDNo63 1402798887
#define _IDNo64 1663632679
#define _IDNo65 1671576103
#define _IDNo66 1408794919
#define _IDNo67 863999783
#define _IDNo68 1131704103
#define _IDNo69 906737442
#define _IDNo70 870631719
#define _IDNo71 1131134247
#define _IDNo72 642094882
#define _IDNo73 640544802
#define _IDNo74 864495655 
#define _IDNo75 640638754 
#define _IDNo76 653320738 
#define _IDNo77 865896231 
#define _IDNo78 1358432263 
#define _IDNo79 1347086855 
#define _IDNo80 1912395529 
#define _IDNo81 1346890759 
#define _IDNo82 1911782409 
#define _IDNo83 1907722761 
#define _IDNo84 1904493833 
#define _IDNo85 1344639239 
#define _IDNo86 1345546503 
#define _IDNo87 1342913799 
#define _IDNo88 1924713764 
#define _IDNo89 1122658340 
#define _IDNo90 642510370 
#define _IDNo91 1916999204 
#define _IDNo92 1118269476 
#define _IDNo93 863412263 
#define _IDNo94 1668822823 
#define _IDNo95 1159891757 
#define _IDNo96 1924134180 
#define _IDNo97 1920996900 
#define _IDNo98 903611949 
#define _IDNo99 1432406573 
#define _IDNo100 652700194 
# define IOT3_START_CARD 1500
# define IOT4_START_CARD 1501
const unsigned long IDNo101 = 3538846747;
const unsigned long IDNo102 = 3531449627;
const unsigned long IDNo103 = 3529356059;
const unsigned long IDNo104 = 3792089883;
const unsigned long IDNo105 = 3527161371;
const unsigned long IDNo106 = 1660892959;
const unsigned long IDNo107 = 3523711515;
const unsigned long IDNo108 = 3528790555;
const unsigned long IDNo109 = 3538846747;
const unsigned long IDNo110 = 2469899802;
const unsigned long IDNo111 = 2478048282;
const unsigned long IDNo112 = 4075658522;
const unsigned long IDNo113 = 2740243738;
const unsigned long IDNo114 = 652368930;
const unsigned long IDNo115 = 2480475162;
const unsigned long IDNo116 = 2072637454;
const unsigned long IDNo117 = 1806499598;
const unsigned long IDNo118 = 1262093071;
const unsigned long IDNo119 = 295462432;
const unsigned long IDNo120 = 2880176399;
const unsigned long IDNo121 = 2478340128;
const unsigned long IDNo122 = 860663323;
const unsigned long IDNo123 = 3685284110;
const unsigned long IDNo124 = 2473339680;
const unsigned long IDNo125 = 194986765;
const unsigned long IDNo126 = 995046413;
const unsigned long IDNo127 = 4070698522;
const unsigned long IDNo128 = 2610622478;
const unsigned long IDNo129 = 1268120590;
const unsigned long IDNo130 = 1001587471;
const unsigned long IDNo131 = 2606181133;
const unsigned long IDNo132 = 3142778638;
const unsigned long IDNo133 = 1807482895;
const unsigned long IDNo134 = 3415634702;
const unsigned long IDNo135 = 1803878415;
const unsigned long IDNo136 = 48157214;
const unsigned long IDNo137 = 555063584;
const unsigned long IDNo138 = 299682080;
const unsigned long IDNo139 = 2209296160;
const unsigned long IDNo140 = 2482707744;
const unsigned long IDNo141 = 2737614874;
const unsigned long IDNo142 = 2470040864;
const unsigned long IDNo143 = 2744751386;
const unsigned long IDNo144 = 2471211808;
const unsigned long IDNo145 = 2202631456;
const unsigned long IDNo146 = 2208332576;
const unsigned long IDNo147 = 2469104922;
const unsigned long IDNo148 = 2474492698;
const unsigned long IDNo149 = 3679648270;
const unsigned long IDNo150 = 4073392157;
const unsigned long IDNo151 = 2467270688;
const unsigned long IDNo152 = 3954503694;
const unsigned long IDNo153 = 2066805774;
const unsigned long IDNo154 = 3008894746;
const unsigned long IDNo155 = 1267331086;
const unsigned long IDNo156 = 2873507597;
const unsigned long IDNo157 = 4065312541;
const unsigned long IDNo158 = 1903381024;
const unsigned long IDNo159 = 1104476448;
const unsigned long IDNo160 = 4063254301;
const unsigned long IDNo161 = 3018101530;
const unsigned long IDNo162 = 2984971552;
const unsigned long IDNo163 = 4062208797;
const unsigned long IDNo164 = 3804429085;
const unsigned long IDNo165 = 2480475212;
const unsigned long IDNo166 = 2213043738;
const unsigned long IDNo167 = 1633276960;
const unsigned long IDNo168 = 45474334;
const unsigned long IDNo169 = 557777952;
const unsigned long IDNo170 = 301561632;
const unsigned long IDNo171 = 2479402528;
const unsigned long IDNo172 = 2480213792;
const unsigned long IDNo173 = 2211773472;
const unsigned long IDNo174 = 2214418208;
const unsigned long IDNo175 = 2205564192;
const unsigned long IDNo176 = 2476560416;
const unsigned long IDNo177 = 305454110;
const unsigned long IDNo178 = 553895712;
const unsigned long IDNo179 = 2481368096;
const unsigned long IDNo180 = 1272643086;
const unsigned long IDNo181 = 302686238;
const unsigned long IDNo182 = 2468710432;
const unsigned long IDNo183 = 2740994330;
const unsigned long IDNo184 = 2481634842;
const unsigned long IDNo185 = 2743197722;
const unsigned long IDNo186 = 2214101792;
const unsigned long IDNo187 = 2478465824;
const unsigned long IDNo188 = 2211033120;
const unsigned long IDNo189 = 25468448;
const unsigned long IDNo190 = 306287902;
const unsigned long IDNo191 = 3951428111;
const unsigned long IDNo192 = 2467858976;
const unsigned long IDNo193 = 2473656096;
const unsigned long IDNo194 = 3150378254;
const unsigned long IDNo195 = 584785051;
const unsigned long IDNo196 = 2334914830;
const unsigned long IDNo197 = 2207734816;
const unsigned long IDNo198 = 3953489166;
const unsigned long IDNo199 = 2210216480;
const unsigned long IDNo200 = 732622862;
const unsigned long IDNo201 = 2078748941;
const unsigned long IDNo202 = 2209067808;
const unsigned long IDNo203 = 2745104410;
const unsigned long IDNo204 = 2472946208;
const unsigned long IDNo205 = 2737594394;
const unsigned long IDNo206 = 2477190432;
const unsigned long IDNo207 = 2207264800;
const unsigned long IDNo208 = 564935968;
const unsigned long IDNo209 = 3143511821;
const unsigned long IDNo210 = 1260721420;
const unsigned long IDNo211 = 1087227934;
const unsigned long IDNo212 = 3808348189;
const unsigned long IDNo213 = 3800661789;
const unsigned long IDNo214 = 2617113358;
const unsigned long IDNo215 = 2879582222;
const unsigned long IDNo216 = 2336816398;
const unsigned long IDNo217 = 1944024858;
const unsigned long IDNo218 = 565396512;
const unsigned long IDNo219 = 293018144;
const unsigned long IDNo220 = 2467292960;
const unsigned long IDNo221 = 2477319456;
const unsigned long IDNo222 = 3687590157;
const unsigned long IDNo223 = 2210505760;
const unsigned long IDNo224 = 332535067;
const unsigned long IDNo225 = 2738946330;
const unsigned long IDNo226 = 1629787674;
const unsigned long IDNo227 = 2467477247;
const unsigned long IDNo228 = 21419808;
const unsigned long IDNo229 = 1263087116;
const unsigned long IDNo230 = 2204818714;
const unsigned long IDNo231 = 1259157005;
const unsigned long IDNo232 = 2205965850;
const unsigned long IDNo233 = 310657822;
const unsigned long IDNo234 = 1093903386;
const unsigned long IDNo235 = 2204944416;
const unsigned long IDNo236 = 656470381;
const unsigned long IDNo237 = 2480578074;
const unsigned long IDNo238 = 291911712;
const unsigned long IDNo239 = 2203251744;
const unsigned long IDNo240 = 4072969245;
const unsigned long IDNo241 = 29705760;
const unsigned long IDNo242 = 2479547168;
const unsigned long IDNo243 = 2482460960;
const unsigned long IDNo244 = 176804619;
const unsigned long IDNo245 = 2741115674;
const unsigned long IDNo246 = 2266806605;
const unsigned long IDNo247 = 3342969421;
const unsigned long IDNo248 = 122726477;
const unsigned long IDNo249 = 1998909517;
const unsigned long IDNo250 = 3621112653;
const unsigned long IDNo251 = 660785229;
const unsigned long IDNo252 = 3354646350;
const unsigned long IDNo253 = 133093454;
const unsigned long IDNo254 = 3072240973;
const unsigned long IDNo255 = 924236366;
const unsigned long IDNo256 = 2004412749;
const unsigned long IDNo257 = 1998841934;
const unsigned long IDNo258 = 3349988941;
const unsigned long IDNo259 = 3879921485;
const unsigned long IDNo260 = 3072904013;
const unsigned long IDNo261 = 3616328013;
const unsigned long IDNo262 = 2277550925;
const unsigned long IDNo263 = 660785485;
const unsigned long IDNo264 = 2003815245;
const unsigned long IDNo265 = 2810628685;
const unsigned long IDNo266 = 2271983437;
const unsigned long IDNo267 = 4154632013;
const unsigned long IDNo268 = 1728426061;
const unsigned long IDNo269 = 1735903054;
const unsigned long IDNo270 = 4153784141;
const unsigned long IDNo271 = 2812987725;
const unsigned long IDNo272 = 131251278;
const unsigned long IDNo273 = 2276964942;
const unsigned long IDNo274 = 2543825997;
const unsigned long IDNo275 = 399821645;
const unsigned long IDNo276 = 3613173581;
const unsigned long IDNo277 = 3621566797;
const unsigned long IDNo278 = 1196602957;
const unsigned long IDNo279 = 2540943437;
const unsigned long IDNo280 = 3343955533;
const unsigned long IDNo281 = 3075057997;
const unsigned long IDNo282 = 924301902;
const unsigned long IDNo283 = 3342060109;
const unsigned long IDNo284 = 2276832589;
const unsigned long IDNo285 = 130411341;
const unsigned long IDNo286 = 936495949;
const unsigned long IDNo287 = 120633421;
const unsigned long IDNo288 = 1736486733;
const unsigned long IDNo289 = 1999027789;
const unsigned long IDNo290 = 2003221070;
const unsigned long IDNo291 = 1468058445;
const unsigned long IDNo292 = 1206040909;
const unsigned long IDNo293 = 118272077;
const unsigned long IDNo294 = 2815342157;
const unsigned long IDNo295 = 870945063;
const unsigned long IDNo296 = 1916059172;
const unsigned long IDNo297 = 1923249188;
const unsigned long IDNo298 = 1125922087;
const unsigned long IDNo299 = 1941270823;
const unsigned long IDNo300 = 1118525476;
const unsigned long IDNo301 = 2743029786;
const unsigned long IDNo302 = 2492238878;
const unsigned long IDNo303 = 1663318567;
const unsigned long IDNo304 = 1382682404;
const unsigned long IDNo305 = 1121142052;
const unsigned long IDNo306 = 2431172259;
const unsigned long IDNo307 = 869740583;
const unsigned long IDNo308 = 1648978463;
const unsigned long IDNo309 = 2748302106;
const unsigned long IDNo310 = 2471800090;
const unsigned long IDNo311 = 2742040090;
const unsigned long IDNo312 = 2737988634;
const unsigned long IDNo313 = 2470332186;
const unsigned long IDNo314 = 2748390938;
const unsigned long IDNo315 = 2474188570;
const unsigned long IDNo316 = 2740608282;
const unsigned long IDNo317 = 2735340058;
const unsigned long IDNo318 = 3528790555;
const unsigned long IDNo319 = 2482662682;
const unsigned long IDNo320 = 2470003994;
const unsigned long IDNo321 = 2476790042;
const unsigned long IDNo322 = 2472246810;
const unsigned long IDNo323 = 2735806490;
const unsigned long IDNo324 = 2740015386;
const unsigned long IDNo325 = 2738401050;
const unsigned long IDNo326 = 2475611930;
const unsigned long IDNo327 = 2475021082;
const unsigned long IDNo328 = 2476383514;
const unsigned long IDNo329 = 2469510938;
const unsigned long IDNo330 = 2735985690;
const unsigned long IDNo331 = 2209318688;
const unsigned long IDNo332 = 2735584794;
const unsigned long IDNo333 = 2474296352;
const unsigned long IDNo334 = 2473924128;
const unsigned long IDNo335 = 2471320352;
const unsigned long IDNo336 = 2480332832;
const unsigned long IDNo337 = 2478523680;
const unsigned long IDNo338 = 2738376986;
const unsigned long IDNo339 = 2477231898;
const unsigned long IDNo340 = 2478283296;
const unsigned long IDNo341 = 2474137632;
const unsigned long IDNo342 = 2208797728;
const unsigned long IDNo343 = 2479058208;
const unsigned long IDNo344 = 846627358;
const unsigned long IDNo345 = 4050518047;
const unsigned long IDNo346 = 2210580512;
const unsigned long IDNo347 = 2475635488;
const unsigned long IDNo348 = 2745516058;
const unsigned long IDNo349 = 2206704416;
const unsigned long IDNo350 = 2205958432;
const unsigned long IDNo351 = 2203993376;
const unsigned long IDNo352 = 3152868878;
const unsigned long IDNo353 = 1802449421;
const unsigned long IDNo354 = 2601595405;
const unsigned long IDNo355 = 29007136;
const unsigned long IDNo356 = 3006317082;
const unsigned long IDNo357 = 4049345567;
const unsigned long IDNo358 = 2737967642;
const unsigned long IDNo359 = 290298400;
const unsigned long IDNo360 = 46238238;
const unsigned long IDNo361 = 317700382;
const unsigned long IDNo362 = 570387744;
const unsigned long IDNo363 = 4226294797;
const unsigned long IDNo364 = 2974754336;
const unsigned long IDNo365 = 2473389344;
const unsigned long IDNo366 = 2921345824;
const unsigned long IDNo367 = 3917014382;
const unsigned long IDNo368 = 2309926040;
const unsigned long IDNo369 = 122612538;
const unsigned long IDNo370 = 1228583320;
const unsigned long IDNo371 = 2277847907;
const unsigned long IDNo372 = 2035401625;
const unsigned long IDNo373 = 2543068770;
const unsigned long IDNo374 = 2189626140;
const unsigned long IDNo375 = 452064523;
const unsigned long IDNo376 = 1226019481;
const unsigned long IDNo377 = 2160897051;
const unsigned long IDNo378 = 2589257483;
const unsigned long IDNo379 = 719579659;
const unsigned long IDNo380 = 3551278362;
const unsigned long IDNo381 = 1250159114;
const unsigned long IDNo382 = 440197898;
const unsigned long IDNo383 = 397828927;
const unsigned long IDNo384 = 3865773850;
const unsigned long IDNo385 = 3343631419;
const unsigned long IDNo386 = 3336293918;
const unsigned long IDNo387 = 2273683008;
const unsigned long IDNo388 = 3600455706;
const unsigned long IDNo389 = 2048977162;
const unsigned long IDNo390 = 1607974440;
const unsigned long IDNo391 = 3621308493;
const unsigned long IDNo392 = 3373592471;
const unsigned long IDNo393 = 1470625850;
const unsigned long IDNo394 = 3072335214;
const unsigned long IDNo395 = 930540397;
const unsigned long IDNo396 = 1329283881;
const unsigned long IDNo397 = 1070081065;
const unsigned long IDNo398 = 339066070;
const unsigned long IDNo399 = 1950497378;
const unsigned long IDNo400 = 79705954;
const unsigned long IDNo401 = 2223387490;
const unsigned long IDNo402 = 883208218;
const unsigned long IDNo403 = 1679832930;
const unsigned long IDNo404 = 604650594;
const unsigned long IDNo405 = 1688943714;
const unsigned long IDNo406 = 2473283610;
const unsigned long IDNo407 = 336607330;
const unsigned long IDNo408 = 1149778274;
const unsigned long IDNo409 = 3035770978;
const unsigned long IDNo410 = 3019977058;
const unsigned long IDNo411 = 3566025058;
const unsigned long IDNo412 = 1148730210;
const unsigned long IDNo413 = 3034329186;
const unsigned long IDNo414 = 1424338714;
const unsigned long IDNo415 = 3291492962;
const unsigned long IDNo416 = 615919970;
const unsigned long IDNo417 = 1689137506;
const unsigned long IDNo418 = 3838718562;
const unsigned long IDNo419 = 2053367098;
const unsigned long IDNo420 = 1247305020;
const unsigned long IDNo421 = 965809366;
const unsigned long IDNo422 = 2310221773;
const unsigned long IDNo423 = 421998038;
const unsigned long IDNo424 = 3926501435;
const unsigned long IDNo425 = 1780005180;
const unsigned long IDNo426 = 1248976186;
const unsigned long IDNo427 = 168929339;
const unsigned long IDNo428 = 706083131;
const unsigned long IDNo429 = 2331090238;
const unsigned long IDNo430 = 3387445705;
const unsigned long IDNo431 = 3403545147;
const unsigned long IDNo432 = 1512228667;
const unsigned long IDNo433 = 178485032;
const unsigned long IDNo434 = 3924567758;
const unsigned long IDNo435 = 3913307601;
const unsigned long IDNo436 = 988307259;
const unsigned long IDNo437 = 310393535;
const unsigned long IDNo438 = 2865557818;
const unsigned long IDNo439 = 2848728270;
const unsigned long IDNo440 = 4202231611;
const unsigned long IDNo441 = 979274281;
const unsigned long IDNo442 = 1785498941;
const unsigned long IDNo443 = 3390529852;
const unsigned long IDNo444 = 3393546044;
const unsigned long IDNo445 = 4209022268;
const unsigned long IDNo446 = 3131363131;
const unsigned long IDNo447 = 692396502;
const unsigned long IDNo448 = 1227017161;
const unsigned long IDNo449 = 1522151996;
const unsigned long IDNo450 = 983049019;
const unsigned long IDNo451 = 981804091;
const unsigned long IDNo452 = 1783753274;
const unsigned long IDNo453 = 3937003322;
const unsigned long IDNo454 = 1774293199;
const unsigned long IDNo455 = 2864894011;
const unsigned long IDNo456 = 699317192;
const unsigned long IDNo457 = 3119468751;
const unsigned long IDNo458 = 446549307;
const unsigned long IDNo459 = 2846134479;
const unsigned long IDNo460 = 972788175;
const unsigned long IDNo461 = 437177659;
const unsigned long IDNo462 = 3376698313;
const unsigned long IDNo463 = 1252680252;
const unsigned long IDNo464 = 1508389070;
const unsigned long IDNo465 = 1501570005;
const unsigned long IDNo466 = 3391082299;
const unsigned long IDNo467 = 450169404;
const unsigned long IDNo468 = 426580437;
const unsigned long IDNo469 = 2061607739;
const unsigned long IDNo470 = 3923254478;
const unsigned long IDNo471 = 2325305640;
const unsigned long IDNo472 = 152551880;
const unsigned long IDNo473 = 3111413967;
const unsigned long IDNo474 = 423064527;
const unsigned long IDNo475 = 3109041870;
const unsigned long IDNo476 = 443118376;
const unsigned long IDNo477 = 4194113494;
const unsigned long IDNo478 = 430473425;
const unsigned long IDNo479 = 3404583226;
const unsigned long IDNo480 = 4198414142;
const unsigned long IDNo481 = 2866797114;
const unsigned long IDNo482 = 3668041274;
const unsigned long IDNo483 = 3659919931;
const unsigned long IDNo484 = 3930257211;
const unsigned long IDNo485 = 2862806587;
const unsigned long IDNo486 = 2848920790;
const unsigned long IDNo487 = 709229371;
const unsigned long IDNo488 = 3653306070;
const unsigned long IDNo489 = 176506940;
const unsigned long IDNo490 = 151104985;
const unsigned long IDNo491 = 700910294;
const unsigned long IDNo492 = 700218831;
const unsigned long IDNo493 = 2320918588;
const unsigned long IDNo494 = 3651008712;
const unsigned long IDNo495 = 3397824316;
const unsigned long IDNo496 = 1767411648;
const unsigned long IDNo497 = 4206575676;
const unsigned long IDNo498 = 3925937723;
const unsigned long IDNo499 = 1776385743;
const unsigned long IDNo500 = 311534797;
const unsigned long IDNo501 = 4187644616;
const unsigned long IDNo502 = 2900237988;
const unsigned long IDNo503 = 1659913941;
const unsigned long IDNo504 = 2191604694;
const unsigned long IDNo505 = 854993877;
const unsigned long IDNo506 = 3531227094;
const unsigned long IDNo507 = 1316918561;
const unsigned long IDNo508 = 1587501857;
const unsigned long IDNo509 = 1118686165;
const unsigned long IDNo510 = 1376256982;
const unsigned long IDNo511 = 1659184086;
const unsigned long IDNo512 = 2725856726;
const unsigned long IDNo513 = 1578747935;
const unsigned long IDNo514 = 3644292764;
const unsigned long IDNo515 = 1046712097;
const unsigned long IDNo516 = 2655780385;
const unsigned long IDNo517 = 1583074849;
const unsigned long IDNo518 = 1508221618;
const unsigned long IDNo519 = 2390919457;
const unsigned long IDNo520 = 2116196129;
const unsigned long IDNo521 = 49621973;
const unsigned long IDNo522 = 2999700437;
const unsigned long IDNo523 = 1656041685;
const unsigned long IDNo524 = 1122772182;
const unsigned long IDNo525 = 3266588885;
const unsigned long IDNo526 = 1384910038;
const unsigned long IDNo527 = 2926694687;
const unsigned long IDNo528 = 1042480673;
const unsigned long IDNo529 = 1116540118;
const unsigned long IDNo530 = 513301281;
const unsigned long IDNo531 = 2727856341;
const unsigned long IDNo532 = 2195797461;
const unsigned long IDNo533 = 2047921723;
const unsigned long IDNo534 = 424697039;
const unsigned long IDNo535 = 3397031229;
const unsigned long IDNo536 = 3651231439;
const unsigned long IDNo537 = 2300087759;
const unsigned long IDNo538 = 4192167631;
const unsigned long IDNo539 = 2060580667;
const unsigned long IDNo540 = 4181460950;
const unsigned long IDNo541 = 1761841361;
const unsigned long IDNo542 = 2572956366;
const unsigned long IDNo543 = 2037526477;
const unsigned long IDNo544 = 712396347;
const unsigned long IDNo545 = 3378261710;
const unsigned long IDNo546 = 1785238057;
const unsigned long IDNo547 = 1507931342;
const unsigned long IDNo548 = 957684941;
const unsigned long IDNo552 = 3273913920;
const unsigned long IDNo553 = 3321994027;
const unsigned long IDNo554 = 337485355;
const unsigned long IDNo555 = 2039078254;
const unsigned long IDNo556 = 3227882034;
const unsigned long IDNo557 = 1192508467;
const unsigned long IDNo558 = 3012844352;
const unsigned long IDNo559 = 347874091;
const unsigned long IDNo560 = 1169635114;
const unsigned long IDNo561 = 3331665963;
const unsigned long IDNo562 = 3237513778;
const unsigned long IDNo563 = 3069080875;
const unsigned long IDNo564 = 1428687402;
const unsigned long IDNo565 = 3005180224;
const unsigned long IDNo566 = 3062594603;
const unsigned long IDNo567 = 1429462826;
const unsigned long IDNo568 = 3504025650;
const unsigned long IDNo569 = 1171892010;
const unsigned long IDNo570 = 3009671744;
const unsigned long IDNo571 = 3491688754;
const unsigned long IDNo572 = 579938356;
const unsigned long IDNo573 = 3326691371;
const unsigned long IDNo574 = 3237598514;
const unsigned long IDNo575 = 3227870258;
const unsigned long IDNo576 = 1438588202;
const unsigned long IDNo577 = 81789483;
const unsigned long IDNo578 = 3068681771;
const unsigned long IDNo579 = 3232566834;
const unsigned long IDNo580 = 3273182272;
const unsigned long IDNo581 = 1170450474;
const unsigned long IDNo582 = 1170345514;
const unsigned long IDNo583 = 2268213052;
const unsigned long IDNo584 = 3017440832;
const unsigned long IDNo585 = 3010189376;
const unsigned long IDNo586 = 3272549440;
const unsigned long IDNo587 = 1426283306;
const unsigned long IDNo588 = 3237377586;
const unsigned long IDNo589 = 3273391680;
const unsigned long IDNo590 = 3015770688;
const unsigned long IDNo591 = 1439106602;
const unsigned long IDNo592 = 3232072242;
const unsigned long IDNo593 = 3009130303;
const unsigned long IDNo594 = 3284365120;
const unsigned long IDNo595 = 1173851690;
const unsigned long IDNo596 = 3337704491;
const unsigned long IDNo597 = 3279095872;
const unsigned long IDNo598 = 3327510059;
const unsigned long IDNo599 = 180112665;
const unsigned long IDNo600 = 3007890496;
const unsigned long IDNo601 = 348905106;









unsigned long rfidConnectTimeLastest = 0;
#define TEMP_STOP_TIME 30000
bool tempStopFlag =false,newCardFlag = true;
unsigned long tempStopFlagTime = 0;
bool stringComplete = false,stringComplete1 = false;
String inputString = "",inputString1 = "";
/*typedef struct{
	unsigned long first;
	unsigned long second;
	bool firstPass , secondPass
}pairCard;
pairCard roundCard;
roundCard.firstPass = false;
roundCard.secondPass = false;
*/
unsigned long remoteRequestTimer = 0;
unsigned long replaceCard(unsigned long cardIn){
        if (cardIn == IDNo407) return _IDNo71;
        if (cardIn == IDNo403) return IDNo199;
        if (cardIn == IDNo395) return IDNo209;
        if (cardIn == IDNo375) return IDNo192;
        if (cardIn == IDNo390) return IDNo164;
	if (cardIn == IDNo379) return IDNo202;
	if (cardIn == IDNo374) return IDNo216;
	if (cardIn == IDNo376) return IDNo305;
	if (cardIn == IDNo378) return _IDNo12;
	if (cardIn == IDNo384) return IDNo125;
	if (cardIn == IDNo212) return _IDNo4;
	if (cardIn == IDNo389) return IDNo102;
	if (cardIn == IDNo385) return IDNo126;
	if (cardIn == IDNo388) return IDNo280;
 	if (cardIn == _IDNo82) return _IDNo21;
	if (cardIn == _IDNo46) return IDNo138;
 	if (cardIn == _IDNo54) return IDNo155;
	if (cardIn == _IDNo57) return _IDNo44;
	if (cardIn == _IDNo97) return _IDNo35;
	if (cardIn == _IDNo98) return _IDNo24;
	if (cardIn == _IDNo96) return _IDNo84;
	if (cardIn == IDNo245) return _IDNo80;
	if (cardIn == _IDNo64) return IDNo190;	
	if (cardIn == IDNo110) return IDNo109;	
	if (cardIn == IDNo106) return IDNo107;	
	if (cardIn == IDNo366) return IDNo109;
	if (cardIn == IDNo370) return IDNo104;
	if (cardIn == IDNo122) return _IDNo56;
	if (cardIn == IDNo243) return IDNo190;	
	if (cardIn == IDNo166) return IDNo139;
	if (cardIn == IDNo196) return IDNo138;
	if (cardIn == IDNo167) return IDNo136;
	if (cardIn == IDNo189) return IDNo178;
	if (cardIn == _IDNo15) return _IDNo5;	
	if (cardIn == IDNo197) return _IDNo27;
	if (cardIn == _IDNo43) return IDNo128;
	if (cardIn == IDNo357) return IDNo128;
	if (cardIn == IDNo242) return IDNo173;	
	if (cardIn == IDNo377) return IDNo124;
	if (cardIn == IDNo313) return IDNo144;
	if (cardIn == IDNo314) return IDNo144;
	if (cardIn == IDNo315) return IDNo144;
	if (cardIn == IDNo360) return _IDNo12;
	if (cardIn == IDNo282) return IDNo156;
	if (cardIn == IDNo284) return IDNo119;
	if (cardIn == IDNo281) return IDNo188;
	if (cardIn == IDNo293) return IDNo149;
	if (cardIn == IDNo286) return _IDNo28;
	if (cardIn == IDNo292) return IDNo161;
	if (cardIn == IDNo383) return _IDNo59;
	if (cardIn == IDNo193) return IDNo221;		
	if (cardIn == IDNo278) return IDNo116;	
	if (cardIn == IDNo279) return IDNo116;
	if (cardIn == IDNo288) return IDNo159;
	if (cardIn == IDNo290) return _IDNo5;
	if (cardIn == IDNo283) return IDNo102;
	if (cardIn == IDNo273) return IDNo148;
	if (cardIn == IDNo295) return IDNo176;	
	if (cardIn == IDNo299) return _IDNo22;
	if (cardIn == IDNo300) return _IDNo4;
	if (cardIn == IDNo306) return IDNo161;
	if (cardIn == IDNo307) return _IDNo40;
	if (cardIn == IDNo309) return IDNo233;
	if (cardIn == IDNo322) return IDNo138;
	if (cardIn == IDNo324) return _IDNo62;	
	if (cardIn == IDNo321) return IDNo103;	
	if (cardIn == IDNo318) return IDNo317;
	if (cardIn == IDNo394) return IDNo181;	
	if (cardIn == IDNo160) return IDNo181;
	if (cardIn == IDNo220) return _IDNo61;
	if (cardIn == IDNo262) return IDNo195;	
	if (cardIn == IDNo246) return IDNo133;
	if (cardIn == IDNo249) return IDNo131;
	if (cardIn == IDNo345) return IDNo200;
	if (cardIn == IDNo348) return IDNo154;
	if (cardIn == IDNo352) return _IDNo30;
	if (cardIn == IDNo340) return IDNo302;
	if (cardIn == IDNo361) return IDNo334;
	if (cardIn == IDNo356) return _IDNo40;
	if (cardIn == IDNo404) return _IDNo42;
	if (cardIn == IDNo338) return IDNo153;
	if (cardIn == _IDNo77) return _IDNo67;
	if (cardIn == _IDNo69) return _IDNo59;
	if (cardIn == IDNo394) return IDNo312;
	if (cardIn == IDNo405) return _IDNo75;
	if (cardIn == _IDNo91) return IDNo389;
	if (cardIn == IDNo396) return _IDNo84;
	if (cardIn == IDNo406) return _IDNo84;
	if (cardIn == IDNo434) return IDNo348;



	return cardIn;
}
/*void checkRound(unsigned long cardIn){
	if (cardIn == roundCard.first) roundCard.firstPass = true;
	if (cardIn == roundCard.second) roundCard.secondPass = true;
	if (roundCard.secondPass == true && roundCard.firstPass == true){
		roundCard.firstPass = false;
		roundCard.secondPass = false;
		int romCound = EEPROM.read(EEPROM_COUNT_ROUND);
		if(romCound <254)
			EEPROM.update(EEPROM_COUNT_ROUND,(romCound +1));
	}
}
*/
bool getDigitalLightSensor(){
  if(digitalRead(DIGITAL_LIGHTSENSOR_PIN) == LOW && digitalLightSensorCount < 10){
    digitalLightSensorCount++;
  }
  else if(digitalRead(DIGITAL_LIGHTSENSOR_PIN) == HIGH ) digitalLightSensorCount = 0;
  
  if(digitalLightSensorCount >= DIGITAL_LIGHTSENSOR_COUNT ){
    digitalLightSensorTimer = millis();
    return false;
  }
  else if(digitalLightSensorCount < DIGITAL_LIGHTSENSOR_COUNT && millis() - digitalLightSensorTimer > DIGITAL_LIGHTSENSOR_RESETTIME){
    return true;
  }
  
}
bool getDigitalLightSensor2(){
  if(digitalRead(DIGITAL_LIGHTSENSOR_PIN2) == LOW && digitalLightSensorCount < 10){
    digitalLightSensorCount++;
  }
  else if(digitalRead(DIGITAL_LIGHTSENSOR_PIN2) == HIGH ) digitalLightSensorCount = 0;
  
  if(digitalLightSensorCount >= DIGITAL_LIGHTSENSOR_COUNT ){
    digitalLightSensorTimer = millis();
    return false;
  }
  else if(digitalLightSensorCount < DIGITAL_LIGHTSENSOR_COUNT && millis() - digitalLightSensorTimer > DIGITAL_LIGHTSENSOR_RESETTIME){
    return true;
  }
  
}

void getOverLoad(bool *leftMotor, bool *rightMotor,int leftPin,int rightPin,int *leftCount,int *rightCount){
  if(digitalRead(leftPin)==LOW && *leftCount < 1000) *leftCount++;
  else *leftCount = 0;
  if(digitalRead(rightPin)==LOW && *rightCount < 1000) *rightCount++;
  else *rightCount = 0;
  
  if(*leftCount >= 4){
    *leftMotor = LOW;
  }
  else{
    *leftMotor = HIGH;
  }
  if(*rightCount >= 4){
    *rightMotor = LOW;
  }
  else{
    *rightMotor = HIGH;
  }
}

double P,I,D;
void getPIDRom(){
	EEPROM.get(EPPROM_P,P);
	EEPROM.get(EPPROM_I,I);
	EEPROM.get(EPPROM_D,D);	
}
void writePIDRom(double _P,double _I,double _D){
	EEPROM.put(EPPROM_P,_P);
	EEPROM.put(EPPROM_I,_I);
	EEPROM.put(EPPROM_D,_D);
}
void writePIDRom_P(double _P){
	EEPROM.put(EPPROM_P,_P);
}
void writePIDRom_I(double _I){
	EEPROM.put(EPPROM_I,_I);
}
void writePIDRom_D(double _D){
	EEPROM.put(EPPROM_D,_D);
}
void printPID(){
	Serial.print("PID: ");
	Serial.print(myPID.GetKp(),4);
	Serial.print(" ");
	Serial.print(myPID.GetKi(),4);
	Serial.print(" ");
	Serial.println(myPID.GetKd(),4);
}
void checkSerial() {
  while (Serial.available()) {
    char inChar = (char)Serial.read();

      Serial.print(inChar);
    inputString1 += inChar;
    if (inChar == '\n') {
      stringComplete1 = true;
      Serial.print(inputString1);
    }
  }
}
double PIDValue;
void updateUART() {
  checkSerial();
  if (stringComplete1) {
    int _length = inputString1.length();
    switch (inputString1[0]) {
      case 'P':
        inputString1 = inputString1.substring(1, _length - 1);
        PIDValue = ((double)inputString1.toInt())/10000.0;
	writePIDRom_P(PIDValue);
	getPIDRom();
	myPID.SetTunings(P,I,D);
	printPID();
	break;
      case 'I':
        inputString1 = inputString1.substring(1, _length - 1);
        PIDValue = ((double)inputString1.toInt())/10000.0;
	writePIDRom_I(PIDValue);
	getPIDRom();
	myPID.SetTunings(P,I,D);
	printPID();
	break;
      case 'D':
        inputString1 = inputString1.substring(1, _length - 1);
        PIDValue = ((double)inputString1.toInt())/10000.0;
	writePIDRom_D(PIDValue);
	getPIDRom();
	myPID.SetTunings(P,I,D);
	printPID();
	break;
        
      
    }
    inputString1 = "";
    stringComplete1 = false;

  }

}
void PIDInit(){
	getPIDRom();
	myPID.SetTunings(P,I,D);
	printPID();
}

const unsigned long MSIOTWrongList[] = {IDNo119,_IDNo5,IDNo190,_IDNo68,IDNo159,IDNo164,IDNo206,IDNo203,IDNo129};
int MSIOTWrongListCount = sizeof(MSIOTWrongList)/sizeof(unsigned long);
bool checkCardMSIOT(unsigned long card){
	if(MSIOTWrongListCount == 0) return false;
	for(int i = 0; i < MSIOTWrongListCount ;i++ ){
		if(MSIOTWrongList[i]==card) return true;
	}
	return false;
}
const unsigned long MSNBWrongList[] = {IDNo190,_IDNo68,IDNo159,IDNo164,IDNo206,IDNo203};
int MSNBWrongListCount = sizeof(MSNBWrongList)/sizeof(unsigned long);
bool checkCardMSNB(unsigned long card){
	if(MSNBWrongListCount == 0) return false;
	for(int i = 0; i < MSNBWrongListCount ;i++ ){
		if(MSNBWrongList[i]==card) return true;
	}
	return false;
}
const unsigned long MSKTrWrongList[] = {_IDNo16,_IDNo5,IDNo190,_IDNo68,IDNo159,IDNo164,IDNo206,IDNo203,IDNo129};
int MSKTrWrongListCount = sizeof(MSKTrWrongList)/sizeof(unsigned long);
bool checkCardMSKTr(unsigned long card){
	if(MSKTrWrongListCount == 0) return false;
	for(int i = 0; i < MSKTrWrongListCount ;i++ ){
		if(MSKTrWrongList[i]==card) return true;
	}
	return false;
}
const unsigned long CRUWrongList[] = {_IDNo42,_IDNo30,IDNo174,IDNo175,IDNo155,IDNo213,IDNo129,IDNo206,IDNo203};
int CRUWrongListCount = sizeof(CRUWrongList)/sizeof(unsigned long);
bool checkCardCRU(unsigned long card){
	if(CRUWrongListCount == 0) return false;
	for(int i = 0; i < CRUWrongListCount ;i++ ){
		if(CRUWrongList[i]==card) return true;
	}
	return false;
}
const unsigned long CRULKBWrongList[] = {_IDNo42,_IDNo30,IDNo121,IDNo143,IDNo155,IDNo213,IDNo129,IDNo206,IDNo203};
int CRULKBWrongListCount = sizeof(CRULKBWrongList)/sizeof(unsigned long);
bool checkCardCRULKB(unsigned long card){
	if(CRULKBWrongListCount == 0) return false;
	for(int i = 0; i < CRULKBWrongListCount ;i++ ){
		if(CRULKBWrongList[i]==card) return true;
	}
	return false;
}
const unsigned long MizuWrongList[] = {IDNo141};
int MizuWrongListCount = sizeof(MizuWrongList)/sizeof(unsigned long);
bool checkCardMizu(unsigned long card){
	if(MizuWrongListCount == 0) return false;
	for(int i = 0; i < MizuWrongListCount ;i++ ){
		if(MizuWrongList[i]==card) return true;
	}
	return false;
}
const unsigned long TMWrongList[] = {IDNo176,IDNo269,IDNo333,IDNo223,IDNo195};
int TMWrongListCount = sizeof(TMWrongList)/sizeof(unsigned long);
bool checkCardTM(unsigned long card){
	if(TMWrongListCount == 0) return false;
	for(int i = 0; i < TMWrongListCount ;i++ ){
		if(TMWrongList[i]==card) return true;
	}
	return false;
}
const unsigned long TM1WrongList[] = {IDNo176,IDNo269,IDNo333,IDNo223,IDNo195};
int TM1WrongListCount = sizeof(TM1WrongList)/sizeof(unsigned long);
bool checkCardTM1(unsigned long card){
	if(TM1WrongListCount == 0) return false;
	for(int i = 0; i < TM1WrongListCount ;i++ ){
		if(TM1WrongList[i]==card) return true;
	}
	return false;
}
const unsigned long KitDaishaWrongList[] = {_IDNo20,IDNo147};
int KitDaishaWrongListCount = sizeof(KitDaishaWrongList)/sizeof(unsigned long);
bool checkCardKitDaisha(unsigned long card){
	if(KitDaishaWrongListCount == 0) return false;
	for(int i = 0; i < KitDaishaWrongListCount ;i++ ){
		if(KitDaishaWrongList[i]==card) return true;
	}
	return false;
}
const unsigned long TonerWrongList[] = {IDNo174,IDNo175,IDNo176,IDNo155,IDNo118,_IDNo76};
int TonerWrongListCount = sizeof(TonerWrongList)/sizeof(unsigned long);
bool checkCardToner(unsigned long card){
	if(TonerWrongListCount == 0) return false;
	for(int i = 0; i < TonerWrongListCount ;i++ ){
		if(TonerWrongList[i]==card) return true;
	}
	return false;
}
const unsigned long LktpWrongList[] = {IDNo224,IDNo305,IDNo188,IDNo223,_IDNo49};
int LktpWrongListCount = sizeof(LktpWrongList)/sizeof(unsigned long);
bool checkCardLktp(unsigned long card){
	if(LktpWrongListCount == 0) return false;
	for(int i = 0; i < LktpWrongListCount ;i++ ){
		if(LktpWrongList[i]==card) return true;
	}
	return false;
}
const unsigned long DgtpWrongList[] = {IDNo224,IDNo310,IDNo301,IDNo180,IDNo174,IDNo175};
int DgtpWrongListCount = sizeof(DgtpWrongList)/sizeof(unsigned long);
bool checkCardDgtp(unsigned long card){
	if(DgtpWrongListCount == 0) return false;
	for(int i = 0; i < DgtpWrongListCount ;i++ ){
		if(DgtpWrongList[i]==card) return true;
	}
	return false;
}
const unsigned long DgtpWrongList1[] = {IDNo224,IDNo301,IDNo174,IDNo175,IDNo168,IDNo170};
int DgtpWrongListCount1 = sizeof(DgtpWrongList1)/sizeof(unsigned long);
bool checkCardDgtp1(unsigned long card){
	if(DgtpWrongListCount1 == 0) return false;
	for(int i = 0; i < DgtpWrongListCount1 ;i++ ){
		if(DgtpWrongList1[i]==card) return true;
	}
	return false;
}
const unsigned long PwbaWrongList[] = {IDNo205,IDNo207,_IDNo68,IDNo294};
int PwbaWrongListCount = sizeof(PwbaWrongList)/sizeof(unsigned long);
bool checkCardPwba(unsigned long card){
	if(PwbaWrongListCount == 0) return false;
	for(int i = 0; i < PwbaWrongListCount ;i++ ){
		if(PwbaWrongList[i]==card) return true;
	}
	return false;
}
const unsigned long PlaWrongList[] = {_IDNo68,IDNo257,IDNo276,_IDNo59,_IDNo76};
int PlaWrongListCount = sizeof(PlaWrongList)/sizeof(unsigned long);
bool checkCardPla(unsigned long card){
	if(PlaWrongListCount == 0) return false;
	for(int i = 0; i < PlaWrongListCount ;i++ ){
		if(PlaWrongList[i]==card) return true;
	}
	return false;
}


#endif