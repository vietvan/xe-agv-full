
#include "Arduino.h"
#include "Mp3.h"

void Mp3::init(int play/*,int next*/)
{
  this->_play = play;
  //this->_next = next;
  pinMode(_play,OUTPUT);
  digitalWrite(_play,OFF);
  delay(LOAD);
  digitalWrite(_play,PRESS);
  delay(DELAY);
  digitalWrite(_play,OFF);
  this->mode = 0;
  this->list = 0;
  this->complete = 1;
}
void Mp3::on(){
  if (this->mode == 0 && this->complete == 1){
    digitalWrite(this->_play,PRESS);
    this->complete = 0;
    this->mode = 1;
    this->count = millis();
  }
}
void Mp3::off(){
  if (this->mode == 1 && this->complete == 1 ){
    digitalWrite(this->_play,PRESS);
    this->complete = 0;
    this->mode = 0;
    this->count = millis();
    
  }
}
void Mp3::check(){
  if ( complete == 0  &&  millis() - count > DELAY  ){
    switch (this->mode)
    {
      case 0:
      case 1:       
        digitalWrite(this->_play,OFF);
        break;
      case 2: 
        digitalWrite(this->_next,OFF);
        this->mode = 1;
        break;
      default: break;
    }
    this->complete = 1;
    }
}
void Mp3::next(){
  if (this->mode == 1 && this->complete == 1){
    digitalWrite(this->_next,PRESS);
    this->complete = 0;
    this->mode = 2;
    this->count = millis();
    }
}
void Mp3::reset(){
    this->mode = 0;
    this->complete = 1;
    digitalWrite(_play,OFF);
    digitalWrite(_next,OFF);
  }



